package com.prizmadigital.shresthatailoring.model;

public class OutletDTO {
    String outlet_id;
    String outlet_name;
    String contact;
    int outlet_image;
    String lat;
    String lon;


    public OutletDTO(String outlet_id, String outlet_name, String contact, int outlet_image, String lat, String lon) {
        this.outlet_id = outlet_id;
        this.outlet_name = outlet_name;
        this.contact = contact;
        this.outlet_image = outlet_image;
        this.lat = lat;
        this.lon = lon;
    }

    public String getOutlet_id() {
        return outlet_id;
    }

    public void setOutlet_id(String outlet_id) {
        this.outlet_id = outlet_id;
    }

    public String getOutlet_name() {
        return outlet_name;
    }

    public void setOutlet_name(String outlet_name) {
        this.outlet_name = outlet_name;
    }

    public String getContact() {
        return contact;
    }

    public void setContact(String contact) {
        this.contact = contact;
    }

    public int getOutlet_image() {
        return outlet_image;
    }

    public void setOutlet_image(int outlet_image) {
        this.outlet_image = outlet_image;
    }

    public String getLat() {
        return lat;
    }

    public void setLat(String lat) {
        this.lat = lat;
    }

    public String getLon() {
        return lon;
    }

    public void setLon(String lon) {
        this.lon = lon;
    }
}
