package com.prizmadigital.shresthatailoring.adapter;

import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.media.MediaMetadataRetriever;
import android.os.Build;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.prizmadigital.shresthatailoring.R;
import com.prizmadigital.shresthatailoring.activity.VideoPlayActivity;


import java.io.File;
import java.util.ArrayList;
import java.util.HashMap;

public class VideoAdapter extends RecyclerView.Adapter<VideoAdapter.MyViewHolder> {

    private ArrayList<String> mImagesList;
    private Context mContext;

    public VideoAdapter(Context context, ArrayList<String> imageList) {
        mContext = context;
        mImagesList = new ArrayList<String>();
        this.mImagesList = imageList;
        // System.out.println("=====> Array path => " + mImagesList.size());
    }


    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.video_list_item, parent, false);

        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, final int position) {

        final String VideoUrl = mImagesList.get(position);
        System.out.println("=====> Array path => " + VideoUrl);


        /*Glide.with(mContext)
                .load("file://" + imageUrl)
                //.placeholder(R.drawable.logo)
                //.error(R.drawable.logo)
                .into(holder.imageView);*/


        /*holder.imageView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent newIntent = new Intent(mContext, ImageGalleyActivity.class);
                newIntent.putStringArrayListExtra("imageList", mImagesList);
                newIntent.putExtra("position", position);
                mContext.startActivity(newIntent);
            }
        });*/

        /*holder.imageView.setOnLongClickListener(new View.OnLongClickListener() {

            @Override
            public boolean onLongClick(View v) {
                //your stuff
                Utilities.shareSelectedImage(mContext,imageUrl);
                return true;
            }
        }); */

        Log.d("Video Url : ", VideoUrl);
        holder.video_name.setText(VideoUrl.substring(VideoUrl.lastIndexOf("/") + 1));

        holder.video_ll.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent newIntent = new Intent(mContext, VideoPlayActivity.class);
                newIntent.putExtra("VideoURL", VideoUrl);
                mContext.startActivity(newIntent);
            }
        });

    }

    @Override
    public int getItemCount() {
        return mImagesList.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {

        public LinearLayout video_ll;
        public ImageView imageView;
        public TextView video_name;

        public MyViewHolder(View view) {
            super(view);
            video_ll = (LinearLayout) view.findViewById(R.id.video_ll);
            imageView = (ImageView) view.findViewById(R.id.imageView);
            video_name = (TextView) view.findViewById(R.id.video_name);
        }
    }
}
