package com.prizmadigital.shresthatailoring.adapter;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.prizmadigital.shresthatailoring.R;
import com.prizmadigital.shresthatailoring.model.CategoryDTO;
import com.prizmadigital.shresthatailoring.model.DashboardDTO;

import java.util.ArrayList;

public class DashboardRecyclerView extends RecyclerView.Adapter<DashboardRecyclerView.ViewHolder> {

    //view

    //variable
    public static String TAG = "categoryRecyclerView";
    Context mContext;
    ArrayList<DashboardDTO> dashboardList = new ArrayList<>();


    public DashboardRecyclerView(Context mContext) {
        this.mContext = mContext;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int i) {
        LayoutInflater inflater = LayoutInflater.from(parent.getContext());
        View view = inflater.inflate(R.layout.dashboard_list_item, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder viewHolder, int position) {
        viewHolder.bindCategory(dashboardList.get(position), position);
    }

    @Override
    public int getItemCount() {
        return dashboardList.size();
    }

    public void addCategoryList(ArrayList<DashboardDTO> list) {
        dashboardList = new ArrayList<>();
        dashboardList.addAll(list);
        notifyDataSetChanged();
    }

    public void clearCategory() {
        int size = dashboardList.size();
        dashboardList.clear();
        //notifyItemRangeRemoved(0, size);
        notifyDataSetChanged();
    }

    public ArrayList<DashboardDTO> returnCategoryList() {
        return dashboardList;
    }


    public class ViewHolder extends RecyclerView.ViewHolder {

        /*
        *
          views for the recycler item
        *
        */


        public ViewHolder(@NonNull View itemView) {
            super(itemView);
        }

        public void bindCategory(DashboardDTO dashboardDTO, final int position) {

        }


    }
}
