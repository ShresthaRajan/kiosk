package com.prizmadigital.shresthatailoring.adapter;

import android.content.Context;
import android.content.Intent;
import android.os.Build;
import android.support.annotation.NonNull;
import android.support.annotation.RequiresApi;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;


import com.prizmadigital.shresthatailoring.R;
import com.prizmadigital.shresthatailoring.activity.DashboardActivity;
import com.prizmadigital.shresthatailoring.activity.ImageListActicvity;
import com.prizmadigital.shresthatailoring.interfaces.ItemClickListener;
import com.prizmadigital.shresthatailoring.model.CategoryDTO;
import com.prizmadigital.shresthatailoring.model.SubCategoryDTO;
import com.prizmadigital.shresthatailoring.utility.PrefUtils;

import java.util.ArrayList;


public class CategoryRecyclerView extends RecyclerView.Adapter<CategoryRecyclerView.ViewHolder> {

    //view

    //variable
    public static String TAG = "categoryRecyclerView";
    Context mContext;
    ArrayList<CategoryDTO> categoryList = new ArrayList<>();
    ItemClickListener clickListener;

    //instance
    SubCategoryRecyclerView subAdapter;


    public CategoryRecyclerView(Context mContext) {
        this.mContext = mContext;
    }

    public CategoryRecyclerView(Context mContext, ArrayList<CategoryDTO> categoryList, ItemClickListener itemClickListener) {
        this.mContext = mContext;
        this.categoryList = categoryList;
        this.clickListener = itemClickListener;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int i) {
        LayoutInflater inflater = LayoutInflater.from(parent.getContext());
        View view = inflater.inflate(R.layout.category_list_item, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder viewHolder, int position) {
        viewHolder.bindCategory(categoryList.get(position), position);
    }

    @Override
    public int getItemCount() {
        return categoryList.size();
    }

    public void addCategoryList(ArrayList<CategoryDTO> list) {
        categoryList = new ArrayList<>();
        categoryList.addAll(list);
        notifyDataSetChanged();
    }

    public void clearCategory() {
        int size = categoryList.size();
        categoryList.clear();
        //notifyItemRangeRemoved(0, size);
        notifyDataSetChanged();
    }

    public ArrayList<CategoryDTO> returnCategoryList() {
        return categoryList;
    }


    public class ViewHolder extends RecyclerView.ViewHolder {

        /*
        *
          views for the recycler item
        *
        */

        LinearLayout category_item;
        ImageView category_imageView;
        ImageView view_all_subcategory;
        RecyclerView subCategory_recyclerView;
        //TextView category_name;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            category_item = (LinearLayout) itemView.findViewById(R.id.category_item);
            category_imageView = (ImageView) itemView.findViewById(R.id.category_imageView);
            view_all_subcategory = (ImageView) itemView.findViewById(R.id.view_all_subcategory);
            subCategory_recyclerView = (RecyclerView) itemView.findViewById(R.id.subCategory_recyclerView);
            //category_name = (TextView) itemView.findViewById(R.id.category_name);
        }

        public void bindCategory(final CategoryDTO categoryDTO, final int position) {

            category_imageView.setImageResource(categoryDTO.getCategoryImage());
            ArrayList<SubCategoryDTO> list = new ArrayList<>();
            list = categoryDTO.getSubCategoryList();

            subAdapter = new SubCategoryRecyclerView(mContext, list, new ItemClickListener() {

                @Override
                public void onCategoryClick(String fileName, ArrayList<SubCategoryDTO> subList) {

                }

                @RequiresApi(api = Build.VERSION_CODES.M)
                @Override
                public void onSubCategoryClick(String fileName) {
                    if (PrefUtils.enabledPermission(mContext)) {
                        Intent newIntent = new Intent(mContext, ImageListActicvity.class);
                        newIntent.putExtra("fileName", fileName);
                        newIntent.putExtra("categoryName", categoryDTO.getCategoryName());
                        mContext.startActivity(newIntent);
                    } else {
                        ((DashboardActivity) mContext).requestRuntimePermission();
                    }

                }

                @Override
                public void longPressClick(String fileName) {

                }
            });

            RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(mContext);
            subCategory_recyclerView.setLayoutManager(mLayoutManager);
            subCategory_recyclerView.setAdapter(subAdapter);

            //category_name.setText(categoryDTO.getCategoryName());

           /* category_item.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    File path = new File(AppText.AppPath);
                    //if(Utilities.checkPathIsAvailable(,path));
                    //Intent newIntent = new Intent(mContext, SubCategoyListActivity.class);
                    //mContext.startActivity(newIntent);
                }
            });*/

            category_item.setOnClickListener(new View.OnClickListener() {
                @RequiresApi(api = Build.VERSION_CODES.M)
                @Override
                public void onClick(View view) {
                    if (PrefUtils.enabledPermission(mContext)) {
                        clickListener.onCategoryClick(categoryDTO.getCategoryName(), categoryDTO.getSubCategoryList());
                    } else {
                        ((DashboardActivity) mContext).requestRuntimePermission();
                    }
                }
            });

            category_imageView.setOnClickListener(new View.OnClickListener() {
                @RequiresApi(api = Build.VERSION_CODES.M)
                @Override
                public void onClick(View view) {
                    if (PrefUtils.enabledPermission(mContext)) {
                        clickListener.onCategoryClick(categoryDTO.getCategoryName(), categoryDTO.getSubCategoryList());
                    } else {
                        ((DashboardActivity) mContext).requestRuntimePermission();
                    }
                }
            });

            view_all_subcategory.setOnClickListener(new View.OnClickListener() {
                @RequiresApi(api = Build.VERSION_CODES.M)
                @Override
                public void onClick(View view) {
                    if (PrefUtils.enabledPermission(mContext)) {
                        clickListener.onCategoryClick(categoryDTO.getCategoryName(), categoryDTO.getSubCategoryList());
                    } else {
                        ((DashboardActivity) mContext).requestRuntimePermission();
                    }
                }
            });

        }


    }

}
