package com.prizmadigital.shresthatailoring.interfaces;

import com.prizmadigital.shresthatailoring.model.SubCategoryDTO;

import java.util.ArrayList;

public interface ItemClickListener {
    public void onCategoryClick(String fileName, ArrayList<SubCategoryDTO> subList);
    public void onSubCategoryClick(String fileName);
    public void longPressClick(String fileName);
}