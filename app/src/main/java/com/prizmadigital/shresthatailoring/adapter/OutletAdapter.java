package com.prizmadigital.shresthatailoring.adapter;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.prizmadigital.shresthatailoring.R;
import com.prizmadigital.shresthatailoring.model.OutletDTO;
import com.prizmadigital.shresthatailoring.utility.Utilities;

import org.w3c.dom.Text;

import java.util.ArrayList;

public class OutletAdapter extends RecyclerView.Adapter<OutletAdapter.MyViewHolder> {

    private ArrayList<OutletDTO> outletList = new ArrayList<>();
    private Context mContext;

    public OutletAdapter(Context context, ArrayList<OutletDTO> list) {
        mContext = context;
        outletList = new ArrayList<OutletDTO>();
        this.outletList = list;
    }


    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.outlet_list_item, parent, false);

        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, final int position) {


        Log.d("Detail of Outlet ", outletList.get(position).getOutlet_name() + "\n" + outletList.get(position).getContact() + "\n" + outletList.get(position).getLat() + "," + outletList.get(position).getLon());
        holder.outlet_imageView.setImageResource(outletList.get(position).getOutlet_image());
        holder.outlet_name.setText(outletList.get(position).getOutlet_name());
        holder.outlet_contact.setText(outletList.get(position).getContact());
        holder.view_map.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String lat = outletList.get(position).getLat();
                String lon = outletList.get(position).getLon();
//                String geo = "geo:" + lat + "," + lon;
//                Uri gmmIntentUri = Uri.parse(geo);
//                Intent mapIntent = new Intent(Intent.ACTION_VIEW, gmmIntentUri);
//                mapIntent.setPackage("com.google.android.apps.maps");
//                mContext.startActivity(mapIntent);

                if (Utilities.isNetworkAvailable(mContext)) {
                    if (Utilities.isGoogleMapsInstalled(mContext)) {
                        String urlAddress = "http://maps.google.com/maps?q=" + lat + "," + lon+ "&z=21" + "(" + outletList.get(position).getOutlet_name() + ")&iwloc=A&hl=es";
                        Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse(urlAddress));
                        mContext.startActivity(intent);

                    } else {
                        Toast.makeText(mContext, "Please install google app to view location in Google Map !!!", Toast.LENGTH_SHORT).show();
                    }
                } else {
                    Toast.makeText(mContext, "Internet Connection is unavailable !!!", Toast.LENGTH_SHORT).show();
                }

            }
        });

    }

    @Override
    public int getItemCount() {
        return outletList.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {

        public ImageView outlet_imageView;
        public TextView outlet_name, outlet_contact, view_map;

        public MyViewHolder(View view) {
            super(view);
            outlet_imageView = (ImageView) view.findViewById(R.id.outlet_imageView);
            outlet_name = (TextView) view.findViewById(R.id.outlet_name);
            outlet_contact = (TextView) view.findViewById(R.id.outlet_contact);
            view_map = (TextView) view.findViewById(R.id.view_map);
        }
    }
}
