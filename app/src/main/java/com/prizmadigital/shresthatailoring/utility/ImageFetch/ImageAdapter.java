package com.prizmadigital.shresthatailoring.utility.ImageFetch;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.bumptech.glide.Glide;
import com.prizmadigital.shresthatailoring.R;
import com.prizmadigital.shresthatailoring.activity.ImageGalleryActivity1;
import com.prizmadigital.shresthatailoring.activity.ImageGalleyActivity;
import com.prizmadigital.shresthatailoring.utility.Utilities;

import java.util.ArrayList;

/**
 * @author Paresh Mayani (@pareshmayani)
 */
public class ImageAdapter extends RecyclerView.Adapter<ImageAdapter.MyViewHolder> {

    private ArrayList<String> mImagesList;
    private Context mContext;

    public ImageAdapter(Context context, ArrayList<String> imageList) {
        mContext = context;
        mImagesList = new ArrayList<String>();
        this.mImagesList = imageList;
        System.out.println("=====> Array path => " + mImagesList.size());
    }

    public void addImage(ArrayList<String> newImages) {
        mImagesList.addAll(newImages);
        notifyDataSetChanged();
    }


    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.image_list_item1, parent, false);

        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, final int position) {

        final String imageUrl = mImagesList.get(position);
        System.out.println("=====> Array path => " + imageUrl);
        Glide.with(mContext)
                .load("file://" + imageUrl)
                //.placeholder(R.drawable.logo)
                //.error(R.drawable.logo)
                .into(holder.imageView);


        holder.imageView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent newIntent = new Intent(mContext, ImageGalleryActivity1.class);
                newIntent.putStringArrayListExtra("imageList", mImagesList);
                newIntent.putExtra("position", position);
                mContext.startActivity(newIntent);
            }
        });

        holder.imageView.setOnLongClickListener(new View.OnLongClickListener() {

            @Override
            public boolean onLongClick(View v) {
                //your stuff
                Utilities.shareSelectedImage(mContext, imageUrl);
                return true;
            }
        });

    }

    @Override
    public int getItemCount() {
        return mImagesList.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {

        public ImageView imageView;

        public MyViewHolder(View view) {
            super(view);
            imageView = (ImageView) view.findViewById(R.id.imageView1);
        }
    }

}
