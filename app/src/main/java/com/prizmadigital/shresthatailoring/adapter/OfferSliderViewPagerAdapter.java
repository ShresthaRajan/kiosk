package com.prizmadigital.shresthatailoring.adapter;

import android.app.Dialog;
import android.content.Context;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;

import com.bumptech.glide.Glide;
//import com.bumptech.glide.load.resource.drawable.GlideDrawable;
import com.bumptech.glide.request.RequestListener;
import com.bumptech.glide.request.target.Target;
import com.prizmadigital.shresthatailoring.R;
import com.prizmadigital.shresthatailoring.model.OfferSliderDTO;
import com.prizmadigital.shresthatailoring.utility.HackyViewPager;
import com.prizmadigital.shresthatailoring.utility.photoview.PhotoView;

import java.util.ArrayList;

public class OfferSliderViewPagerAdapter extends PagerAdapter {
    private Context context;
    private LayoutInflater layoutInflater;
    ArrayList<OfferSliderDTO> sliderList = new ArrayList<>();


    public OfferSliderViewPagerAdapter(ArrayList<OfferSliderDTO> sliderImages, Context context) {
        this.context = context;
        this.sliderList = sliderImages;
    }


    @Override
    public int getCount() {
        return sliderList.size();
    }

    @Override
    public boolean isViewFromObject(View view, Object object) {
        return view == object;
    }

    @Override
    public Object instantiateItem(ViewGroup container, final int position) {

        layoutInflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View view = layoutInflater.inflate(R.layout.ads_slider_list_item, null);
        ImageView imageView = (ImageView) view.findViewById(R.id.slider_image);
        RelativeLayout slider_rl = (RelativeLayout) view.findViewById(R.id.slider_rl);

        if (sliderList.get(position).getOffer_id().equalsIgnoreCase("1")) {
            imageView.setImageResource(sliderList.get(position).getOffer_image());
        } else {
            Log.d("Banner path ====> ", sliderList.get(position).getBannerpath());
            Glide.with(context)
                    .load(sliderList.get(position).getBannerpath())
                    //.placeholder(R.drawable.logo)
                    //.error(R.drawable.logo)
                    .into(imageView);
        }


        slider_rl.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showPopUp(sliderList, position);
            }
        });

        ViewPager vp = (ViewPager) container;
        vp.addView(view, 0);
        return view;

    }

    private void showPopUp(ArrayList<OfferSliderDTO> sliderList, int position) {

        final Dialog d = new Dialog(context);
        d.setContentView(R.layout.ads_dialog_layout);
        d.getWindow().setBackgroundDrawableResource(android.R.color.transparent);

        final HackyViewPager popup_viewPager = (HackyViewPager) d.findViewById(R.id.popup_viewPager);
        final ImageView close = (ImageView) d.findViewById(R.id.close);

        if (sliderList != null) {

            popup_viewPager.setAdapter(new SliderAdsPageAdapter(context, sliderList));
            popup_viewPager.setCurrentItem(position);

            popup_viewPager.setOnPageChangeListener(new ViewPager.OnPageChangeListener() {
                @Override
                public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {
                }

                @Override
                public void onPageSelected(int position) {

                }

                @Override
                public void onPageScrollStateChanged(int state) {

                }
            });

        }

        Window dialogWindow = d.getWindow();
        WindowManager.LayoutParams lp = dialogWindow.getAttributes();
        dialogWindow.setGravity(Gravity.CENTER_HORIZONTAL | Gravity.TOP);

        lp.y = 300; // The new position of the Y coordinates

        lp.width = WindowManager.LayoutParams.MATCH_PARENT; // Width
        lp.height = WindowManager.LayoutParams.WRAP_CONTENT; // Height

        // The system will call this function when the Window Attributes when the change, can be called directly by application of above the window parameters change, also can use setAttributes
        d.onWindowAttributesChanged(lp);
        dialogWindow.setAttributes(lp);

        d.show();

        close.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                d.dismiss();
            }
        });

    }


    public class SliderAdsPageAdapter extends PagerAdapter {

        //variables
        private ArrayList<OfferSliderDTO> list;

        private LayoutInflater inflater;
        private Context mContext;

        //views
        View myImageLayout = null;

        public SliderAdsPageAdapter(Context context, ArrayList<OfferSliderDTO> imgList) {
            this.mContext = context;
            this.list = imgList;
            inflater = LayoutInflater.from(context);
        }


        @Override
        public void destroyItem(ViewGroup container, int position, Object object) {
            container.removeView((View) object);
        }

        @Override
        public int getCount() {
            return list.size();
        }


        @Override
        public Object instantiateItem(ViewGroup view, int position) {


            myImageLayout = inflater.inflate(R.layout.view_pager_list_item1, view, false);
            PhotoView myImage = myImageLayout.findViewById(R.id.image);


            final ProgressBar progressBar = (ProgressBar) myImageLayout.findViewById(R.id.activitiesProgressBar);
            myImage.setScaleType(ImageView.ScaleType.FIT_XY);
            progressBar.setVisibility(View.GONE);

            if (list.get(position).getOffer_id().equalsIgnoreCase("1")) {
                Glide.with(mContext)
                        .load(list.get(position).getOffer_image())
                        //.placeholder(R.drawable.logo)
                        //.error(R.drawable.logo)
                        .into(myImage);
            } else {
                Glide.with(mContext)
                        .load(list.get(position).getBannerpath())
                        //.placeholder(R.drawable.logo)
                        //.error(R.drawable.logo)
                        .into(myImage);
            }


            /*Glide.with(mContext).load(list.get(position).getOffer_image()).error(R.drawable.logo).listener(new RequestListener<String, GlideDrawable>() {
                @Override
                public boolean onException(Exception e, String model, Target<GlideDrawable> target, boolean isFirstResource) {
                    progressBar.setVisibility(View.GONE);
                    return false;
                }

                @Override
                public boolean onResourceReady(GlideDrawable resource, String model, Target<GlideDrawable> target, boolean isFromMemoryCache, boolean isFirstResource) {
//                   Log.d(TAG,"Visibility: "+progressBar.getVisibility());
                    progressBar.setVisibility(View.GONE);
                    return false;
                }
            }).into(myImage);*/


            view.addView(myImageLayout, 0);


            return myImageLayout;
        }

        @Override
        public boolean isViewFromObject(View view, Object object) {
            return view.equals(object);
        }

    }


    @Override
    public void destroyItem(ViewGroup container, int position, Object object) {

        ViewPager vp = (ViewPager) container;
        View view = (View) object;
        vp.removeView(view);

    }

}

