package com.prizmadigital.shresthatailoring.activity;

import android.content.Intent;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.provider.MediaStore;
import android.support.annotation.Nullable;
import android.support.design.widget.Snackbar;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.prizmadigital.shresthatailoring.R;
import com.prizmadigital.shresthatailoring.utility.AppText;
import com.prizmadigital.shresthatailoring.utility.ImageFetch.ImageAdapter;
import com.prizmadigital.shresthatailoring.utility.ImageFetch.ItemOffsetDecoration;
import com.prizmadigital.shresthatailoring.utility.Utilities;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;

import static android.Manifest.permission.READ_EXTERNAL_STORAGE;

public class FeatureProductActivity extends AppCompatActivity {

    //view
    ImageView back;
    TextView activity_name;
    TextView no_image_textView;
    RecyclerView feature_product_recyclerView;

    //instance
    ImageAdapter imageAdapter;

    //variable
    private static final int REQUEST_FOR_STORAGE_PERMISSION = 123;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_feature_product);

        initView();
        try {
            populateImagesFromGallery();
        } catch (IOException e) {
            e.printStackTrace();
        }

        clickMethods();
        resetDisconnectTimer();
    }


    private ArrayList<String> fetchFiles() throws IOException {

        ArrayList<String> filenames = new ArrayList<String>();
        String path = AppText.AppPath + "/" + "Feature Product/";

        Log.d("PATH =======> ", path);

        File file = new File(path);
        Utilities.checkPathIsAvailable(FeatureProductActivity.this, file);

        File directory = new File(path);
        File[] files = directory.listFiles();

        Log.d("files =======> ", "" + files.length);
        if (file.length() > 0) {
            no_image_textView.setVisibility(View.GONE);
            feature_product_recyclerView.setVisibility(View.VISIBLE);
            for (int i = 0; i < files.length; i++) {

                String file_name = path + files[i].getName();
                Log.d("FileName ===> ", files[i].getName());
                // you can store name to arraylist and use it later
                filenames.add(file_name);
            }
        }

        return filenames;
    }

    //For Fetching Image into Grid Form
    private void populateImagesFromGallery() throws IOException {
        if (!mayRequestGalleryImages()) {
            return;
        }

        ArrayList<String> imageUrls = fetchFiles();
        if (imageUrls.size() > 0) {
            feature_product_recyclerView.setVisibility(View.VISIBLE);
            no_image_textView.setVisibility(View.GONE);
            initializeRecyclerView(imageUrls);
        } else {
            feature_product_recyclerView.setVisibility(View.GONE);
            no_image_textView.setVisibility(View.VISIBLE);
        }

    }

    private void initializeRecyclerView(ArrayList<String> imageUrls) {
        imageAdapter = new ImageAdapter(this, imageUrls);

        RecyclerView.LayoutManager layoutManager = new GridLayoutManager(getApplicationContext(), 7);

        feature_product_recyclerView.setLayoutManager(layoutManager);
        feature_product_recyclerView.setItemAnimator(new DefaultItemAnimator());
        feature_product_recyclerView.addItemDecoration(new ItemOffsetDecoration(this, R.dimen.item_offset));
        feature_product_recyclerView.setAdapter(imageAdapter);
    }


    private boolean mayRequestGalleryImages() {

        if (Build.VERSION.SDK_INT < Build.VERSION_CODES.M) {
            return true;
        }

        if (checkSelfPermission(READ_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED) {
            return true;
        }

        if (shouldShowRequestPermissionRationale(READ_EXTERNAL_STORAGE)) {
            //promptStoragePermission();
            showPermissionRationaleSnackBar();
        } else {
            requestPermissions(new String[]{READ_EXTERNAL_STORAGE}, REQUEST_FOR_STORAGE_PERMISSION);
        }

        return false;
    }

    private void showPermissionRationaleSnackBar() {
        Snackbar.make(findViewById(R.id.back), getString(R.string.permission_rationale),
                Snackbar.LENGTH_INDEFINITE).setAction(getResources().getString(R.string.OK), new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                // Request the permission
                ActivityCompat.requestPermissions(FeatureProductActivity.this,
                        new String[]{READ_EXTERNAL_STORAGE},
                        REQUEST_FOR_STORAGE_PERMISSION);
            }
        }).show();

    }


    private Handler disconnectHandler = new Handler(new Handler.Callback() {
        @Override
        public boolean handleMessage(Message msg) {
            // todo
            return true;
        }
    });

    private Runnable disconnectCallback = new Runnable() {
        @Override
        public void run() {
            // Perform any required operation on disconnect
            Intent newIntent = new Intent(FeatureProductActivity.this, AdsActivity.class);
            startActivity(newIntent);
        }
    };

    public void resetDisconnectTimer() {
        disconnectHandler.removeCallbacks(disconnectCallback);
        disconnectHandler.postDelayed(disconnectCallback, AppText.DISCONNECT_TIMEOUT);
    }

    public void stopDisconnectTimer() {
        disconnectHandler.removeCallbacks(disconnectCallback);
    }

    @Override
    public void onUserInteraction() {
        resetDisconnectTimer();
    }

    @Override
    public void onResume() {
        super.onResume();
        resetDisconnectTimer();
    }

    @Override
    public void onStop() {
        super.onStop();
        stopDisconnectTimer();
    }

    private void initView() {

        feature_product_recyclerView = (RecyclerView) findViewById(R.id.feature_product_recyclerView);
        activity_name = (TextView) findViewById(R.id.activity_name);
        activity_name.setText("Feature Product");
        no_image_textView = (TextView) findViewById(R.id.no_image_textView);
        back = (ImageView) findViewById(R.id.back);
    }

    private void clickMethods() {

        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
    }


}
