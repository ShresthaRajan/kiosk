package com.prizmadigital.shresthatailoring.activity;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.resource.drawable.GlideDrawable;
import com.bumptech.glide.request.RequestListener;
import com.bumptech.glide.request.target.Target;
import com.prizmadigital.shresthatailoring.R;
import com.prizmadigital.shresthatailoring.ThumbIndicator;
import com.prizmadigital.shresthatailoring.adapter.ScrollListAdapter;
import com.prizmadigital.shresthatailoring.adapter.ViewPageAdapter;
import com.prizmadigital.shresthatailoring.utility.AppText;
import com.prizmadigital.shresthatailoring.utility.HackyViewPager;
import com.prizmadigital.shresthatailoring.utility.photoview.PhotoView;

import java.util.ArrayList;

public class ImageGalleryActivity1 extends AppCompatActivity {

    //instance
    Intent intent;
    ViewPageAdapter viewPageAdapter;
    VpAdapter adp;


    //Variables
    int position;
    ArrayList<String> imageList = new ArrayList<>();
    private int indexToDelete = -1;

    //views
    ImageView back;
    TextView activity_name;
    private HackyViewPager gallery_viewPager;
    ThumbIndicator indicator;


    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_image_gallery1);

        intent = getIntent();

        position = intent.getIntExtra("position", 0);
        imageList = intent.getStringArrayListExtra("imageList");


        back = (ImageView) findViewById(R.id.back);
        activity_name = (TextView) findViewById(R.id.activity_name);
        activity_name.setVisibility(View.GONE);
        gallery_viewPager = (HackyViewPager) findViewById(R.id.gallery_viewPager);
        indicator = (ThumbIndicator) findViewById(R.id.indicator);

        setGalleyList(imageList, position);

        resetDisconnectTimer();

        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });

        setup();
    }

    private void setup() {
        adp = new VpAdapter();
        gallery_viewPager.setAdapter(adp);
        indicator.setupWithViewPager(gallery_viewPager, (ArrayList<String>) imageList, 100);
    }

    @Override
    protected void onStart() {
        super.onStart();
        //position = intent.getIntExtra("position", 0);
        gallery_viewPager.setCurrentItem(position);
        gallery_viewPager.setOffscreenPageLimit(5);
    }

    private Handler disconnectHandler = new Handler(new Handler.Callback() {
        @Override
        public boolean handleMessage(Message msg) {
            // todo
            return true;
        }
    });

    private Runnable disconnectCallback = new Runnable() {
        @Override
        public void run() {
            // Perform any required operation on disconnect
            Intent newIntent = new Intent(ImageGalleryActivity1.this, AdsActivity.class);
            startActivity(newIntent);
        }
    };

    public void resetDisconnectTimer() {
        disconnectHandler.removeCallbacks(disconnectCallback);
        disconnectHandler.postDelayed(disconnectCallback, AppText.DISCONNECT_TIMEOUT);
    }

    public void stopDisconnectTimer() {
        disconnectHandler.removeCallbacks(disconnectCallback);
    }

    @Override
    public void onUserInteraction() {
        resetDisconnectTimer();
    }

    @Override
    public void onResume() {
        super.onResume();
        resetDisconnectTimer();
    }

    @Override
    public void onStop() {
        super.onStop();
        stopDisconnectTimer();
    }


    private void setGalleyList(final ArrayList<String> imageList, int position) {

        if (imageList != null) {

            /*gallery_viewPager.setAdapter(new ViewPageAdapter(ImageGalleryActivity1.this, imageList));
             *//*gallery_viewPager.setCurrentItem(position);
            gallery_viewPager.setOffscreenPageLimit(5);*//*


            gallery_viewPager.setOnPageChangeListener(new ViewPager.OnPageChangeListener() {
                @Override
                public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

                }

                @Override
                public void onPageSelected(int position) {

                }

                @Override
                public void onPageScrollStateChanged(int scrollState) {
                    *//*if (scrollState != ViewPager.SCROLL_STATE_IDLE) {
                        final int childCount = gallery_viewPager.getChildCount();
                        for (int i = 0; i < childCount; i++)
                            gallery_viewPager.getChildAt(i).setLayerType(View.LAYER_TYPE_NONE, null);
                    }*//*

                    if (indexToDelete != -1 && scrollState == ViewPager.SCROLL_STATE_IDLE) {
                        imageList.remove(indexToDelete);
                        adp.notifyDataSetChanged();
                        indicator.notifyDataSetChanged();
                        if (indexToDelete == 0) {
                            gallery_viewPager.setCurrentItem(indexToDelete, false);
                            indicator.setCurrentItem(indexToDelete, false);
                        }
                        indexToDelete = -1;
                    }
                }
            });*/


            gallery_viewPager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
                @Override
                public void onPageScrolled(int i, float v, int i1) {

                }

                @Override
                public void onPageSelected(int i) {
                }

                @Override
                public void onPageScrollStateChanged(int i) {
                    if (indexToDelete != -1 && i == ViewPager.SCROLL_STATE_IDLE) {
                        imageList.remove(indexToDelete);
                        adp.notifyDataSetChanged();
                        indicator.notifyDataSetChanged();
                        if (indexToDelete == 0) {
                            gallery_viewPager.setCurrentItem(indexToDelete, false);
                            indicator.setCurrentItem(indexToDelete, false);
                        }
                        indexToDelete = -1;
                    }
                }
            });
        }
    }


    class VpAdapter extends PagerAdapter {
        @NonNull
        @Override
        public Object instantiateItem(@NonNull ViewGroup container, final int position) {
            View v = getLayoutInflater().inflate(R.layout.view_pager_list_item1, container, false);
            PhotoView imgSlider = v.findViewById(R.id.image);
            final ProgressBar progressBar = (ProgressBar) v.findViewById(R.id.activitiesProgressBar);
            //Glide.with(container.getContext()).load(imageList.get(position)).into(imgSlider);
            Glide.with(container.getContext()).load("file://" + imageList.get(position)).error(R.drawable.logo).listener(new RequestListener<String, GlideDrawable>() {
                @Override
                public boolean onException(Exception e, String model, Target<GlideDrawable> target, boolean isFirstResource) {
                    progressBar.setVisibility(View.GONE);
                    return false;
                }

                @Override
                public boolean onResourceReady(GlideDrawable resource, String model, Target<GlideDrawable> target, boolean isFromMemoryCache, boolean isFirstResource) {
//                   Log.d(TAG,"Visibility: "+progressBar.getVisibility());
                    progressBar.setVisibility(View.GONE);
                    return false;
                }
            }).into(imgSlider);
            container.addView(v);
            return v;
        }

        @Override
        public int getItemPosition(@NonNull Object object) {
            if (imageList.indexOf(object) == -1)
                return POSITION_NONE;
            else
                return super.getItemPosition(object);
        }

        @Override
        public boolean isViewFromObject(@NonNull View view, @NonNull Object o) {
            return view == o;
        }

        @Override
        public void destroyItem(@NonNull ViewGroup container, int position, @NonNull Object object) {
            container.removeView((View) object);
        }

        @Override
        public int getCount() {
            return imageList.size();
        }
    }

}
