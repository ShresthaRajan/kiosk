package com.prizmadigital.shresthatailoring.adapter;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.prizmadigital.shresthatailoring.R;
import com.prizmadigital.shresthatailoring.interfaces.ItemClickListener;

import java.util.ArrayList;

public class FileRecyclerView extends RecyclerView.Adapter<FileRecyclerView.ViewHolder> {
    //view

    //variable
    public static String TAG = "categoryRecyclerView";
    Context mContext;
    ArrayList<String> filenamesList = new ArrayList<>();
    ItemClickListener clickListener;


    public FileRecyclerView(Context mContext) {
        this.mContext = mContext;
    }

    public FileRecyclerView(Context mContext, ArrayList<String> filenames, ItemClickListener itemClickListener) {
        this.mContext = mContext;
        this.filenamesList = filenames;
        this.clickListener = itemClickListener;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int i) {
        LayoutInflater inflater = LayoutInflater.from(parent.getContext());
        View view = inflater.inflate(R.layout.foler_list_item, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder viewHolder, int position) {
        viewHolder.bindCategory(filenamesList.get(position), position);
    }

    @Override
    public int getItemCount() {
        return filenamesList.size();
    }

    public void addfileList(ArrayList<String> list) {
        filenamesList = new ArrayList<>();
        filenamesList.addAll(list);
        notifyDataSetChanged();
    }

    public void clearCategory() {
        int size = filenamesList.size();
        filenamesList.clear();
        //notifyItemRangeRemoved(0, size);
        notifyDataSetChanged();
    }

    public ArrayList<String> returnCategoryList() {
        return filenamesList;
    }


    public class ViewHolder extends RecyclerView.ViewHolder {

        /*
        *
          views for the recycler item
        *
        */

        LinearLayout folder_item;
        TextView filename_textView;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            folder_item = (LinearLayout) itemView.findViewById(R.id.folder_item);
            filename_textView = (TextView) itemView.findViewById(R.id.filename_textView);
        }

        public void bindCategory(final String fileName, final int position) {

            filename_textView.setText(fileName);

           /* category_item.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    File path = new File(AppText.AppPath);
                    //if(Utilities.checkPathIsAvailable(,path));
                    //Intent newIntent = new Intent(mContext, SubCategoyListActivity.class);
                    //mContext.startActivity(newIntent);
                }
            });*/

            folder_item.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    clickListener.onSubCategoryClick(fileName);
                }
            });

            folder_item.setOnLongClickListener(new View.OnLongClickListener() {
                public boolean onLongClick(View arg0) {
                    clickListener.longPressClick(fileName);
                    return false;
                }
            });
        }


    }

}
