package com.prizmadigital.shresthatailoring.utility;

import android.os.Environment;

import java.io.File;

public class AppText {

    public static String AppPath = Environment.getExternalStorageDirectory()
            + File.separator + "ShresthaTailor";
    public static final long DISCONNECT_TIMEOUT = 300000; // 5 min = 5 * 60 * 1000 ms

    public static final String categoryResponse = "[\n" +
            "{ \n" +
            "   \"categoryId\":\"1\",\n" +
            "   \"categoryName\":\"Suits\",\n" +
            "   \"categoryImage\":\"\",\n" +
            "   \"subcategory\":[ \n" +
            "      { \n" +
            "         \"subcategoryId\":\"1\",\n" +
            "         \"subcategoryName\":\"Slim Fit\"\n" +
            "      },\n" +
            "      { \n" +
            "         \"subcategoryId\":\"2\",\n" +
            "         \"subcategoryName\":\"Regular Fit\"\n" +
            "      },\n" +
            "      { \n" +
            "         \"subcategoryId\":\"3\",\n" +
            "         \"subcategoryName\":\"Waist Coat\"\n" +
            "      },\n" +
            "      { \n" +
            "         \"subcategoryId\":\"4\",\n" +
            "         \"subcategoryName\":\"Tuxedo\"\n" +
            "      },\n" +
            "      { \n" +
            "         \"subcategoryId\":\"5\",\n" +
            "         \"subcategoryName\":\"Ladies Suits\"\n" +
            "      }\n" +
            "   ]\n" +
            "},\n" +
            "{\n" +
            "    \"categoryId\": \"2\",\n" +
            "    \"categoryName\": \"Blazers\",\n" +
            "    \"categoryImage\": \"\",\n" +
            "    \"subcategory\": [\n" +
            "      {\n" +
            "        \"subcategoryId\": \"1\",\n" +
            "        \"subcategoryName\": \"Blazer\"\n" +
            "      },\n" +
            "      {\n" +
            "        \"subcategoryId\": \"2\",\n" +
            "        \"subcategoryName\": \"Top Coat\"\n" +
            "      },\n" +
            "      {\n" +
            "        \"subcategoryId\": \"3\",\n" +
            "        \"subcategoryName\": \"Juwari Coat\"\n" +
            "      },\n" +
            "      {\n" +
            "        \"subcategoryId\": \"4\",\n" +
            "        \"subcategoryName\": \"Dr. Apron\"\n" +
            "      }\n" +
            "    ]\n" +
            "  },\n" +
            "{\n" +
            "    \"categoryId\": \"3\",\n" +
            "    \"categoryName\": \"Shirts\",\n" +
            "    \"categoryImage\": \"\",\n" +
            "    \"subcategory\": [\n" +
            "      {\n" +
            "        \"subcategoryId\": \"1\",\n" +
            "        \"subcategoryName\": \"Formal Shirts\"\n" +
            "      }\n" +
            "    ]\n" +
            "  },\n" +
            "{\n" +
            "    \"categoryId\": \"4\",\n" +
            "    \"categoryName\": \"Pants\",\n" +
            "    \"categoryImage\": \"\",\n" +
            "    \"subcategory\": [\n" +
            "      {\n" +
            "        \"subcategoryId\": \"1\",\n" +
            "        \"subcategoryName\": \"Formal Pants\"\n" +
            "      }\n" +
            "    ]\n" +
            "  },\n" +
            "{\n" +
            "    \"categoryId\": \"5\",\n" +
            "    \"categoryName\": \"Daura Surwal\",\n" +
            "    \"categoryImage\": \"\",\n" +
            "    \"subcategory\": [\n" +
            "      {\n" +
            "        \"subcategoryId\": \"1\",\n" +
            "        \"subcategoryName\": \"Wedding\"\n" +
            "      },\n" +
            "      {\n" +
            "        \"subcategoryId\": \"2\",\n" +
            "        \"subcategoryName\": \"Normal\"\n" +
            "      }\n" +
            "    ]\n" +
            "  },\n" +
            "{\n" +
            "    \"categoryId\": \"5\",\n" +
            "    \"categoryName\": \"Kids\",\n" +
            "    \"categoryImage\": \"\",\n" +
            "    \"subcategory\": [\n" +
            "      {\n" +
            "        \"subcategoryId\": \"1\",\n" +
            "        \"subcategoryName\": \"Suits\"\n" +
            "      },\n" +
            "      {\n" +
            "        \"subcategoryId\": \"2\",\n" +
            "        \"subcategoryName\": \"Daura Surwal\"\n" +
            "      }\n" +
            "    ]\n" +
            "  }\n" +
            "]";

}
