package com.prizmadigital.shresthatailoring.interfaces;

import android.view.View;

public interface RecylerItemClickListner {
    void onClick(View view, int position);
}
