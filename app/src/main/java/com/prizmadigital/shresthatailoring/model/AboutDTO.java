package com.prizmadigital.shresthatailoring.model;

public class AboutDTO {

    String about_id;
    int about_image;

    public AboutDTO(String about_id, int about_image) {
        this.about_id = about_id;
        this.about_image = about_image;
    }

    public String getAbout_id() {
        return about_id;
    }

    public void setAbout_id(String about_id) {
        this.about_id = about_id;
    }

    public int getAbout_image() {
        return about_image;
    }

    public void setAbout_image(int about_image) {
        this.about_image = about_image;
    }
}
