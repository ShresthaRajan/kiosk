package com.prizmadigital.shresthatailoring.activity;

import android.content.Intent;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.provider.MediaStore;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.Snackbar;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.prizmadigital.shresthatailoring.R;
import com.prizmadigital.shresthatailoring.utility.AppText;
import com.prizmadigital.shresthatailoring.utility.DataHolder;
import com.prizmadigital.shresthatailoring.utility.ImageFetch.ImageAdapter;
import com.prizmadigital.shresthatailoring.utility.ImageFetch.ItemOffsetDecoration;
import com.prizmadigital.shresthatailoring.utility.Utilities;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;

import static android.Manifest.permission.READ_EXTERNAL_STORAGE;

public class ImageListActicvity extends AppCompatActivity {

    //view
    /*fro toolbae section*/
    ImageView back;
    TextView activity_name;
    Button loadMore;

    //
    public static TextView no_image_textView;
    public static RecyclerView image_recyclerView;

    //instance
    Intent intent;
    ImageAdapter imageAdapter;
    //ImageAdapter1 imageAdapter;

    //variable
    String subCategoryName, categoryName;
    Paginator p = null;
    private int totalPages = 0;
    private int currentPage = 0;
    private boolean isLastPage = false;
    private boolean isLoading = false;
    int itemCount = 0;
    ArrayList<String> totalImageList = new ArrayList<>();

    private static final int REQUEST_FOR_STORAGE_PERMISSION = 123;


    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_image_list);

        initView();
        try {
            populateImagesFromGallery();
        } catch (IOException e) {
            e.printStackTrace();
        }

        clickMethods();
        resetDisconnectTimer();
    }

    private ArrayList<String> fetchFiles() throws IOException {

        ArrayList<String> filenames = new ArrayList<String>();
        String path = AppText.AppPath + "/" + categoryName + "/" + subCategoryName + "/";

        //Log.d("PATH =======> ", path);

        File file = new File(path);
        Utilities.checkPathIsAvailable(ImageListActicvity.this, file);

        File directory = new File(path);
        File[] files = directory.listFiles();

        //Log.d("files =======> ", "" + files.length);
        if (file.length() > 0) {
            no_image_textView.setVisibility(View.GONE);
            image_recyclerView.setVisibility(View.VISIBLE);
            for (int i = 0; i < files.length; i++) {

                String file_name = path + files[i].getName();
                //Log.d("FileName ===> ", files[i].getName());
                // you can store name to arraylist and use it later
                filenames.add(file_name);
            }

            //Toast.makeText(this, "ImageSize : "+ filenames.size(), Toast.LENGTH_SHORT).show();
        }

        return filenames;
    }

    //For Fetching Image into Grid Form
    private void populateImagesFromGallery() throws IOException {
        if (!mayRequestGalleryImages()) {
            return;
        }

        //ArrayList<String> imageUrls = loadPhotosFromNativeGallery();
        ArrayList<String> imageUrls = DataHolder.fetchFiles(categoryName, subCategoryName, ImageListActicvity.this);
        //Toast.makeText(this, "Length : " + imageUrls.size(), Toast.LENGTH_SHORT).show();
        if (imageUrls.size() > 0) {
            image_recyclerView.setVisibility(View.VISIBLE);
            no_image_textView.setVisibility(View.GONE);
            initializeRecyclerView(imageUrls);
        } else {
            image_recyclerView.setVisibility(View.GONE);
            no_image_textView.setVisibility(View.VISIBLE);
        }

    }

    private void initializeRecyclerView(ArrayList<String> imageUrls) {
        //imageAdapter = new ImageAdapter(this, imageUrls);
        try {
            imageAdapter = new ImageAdapter(this, p.getCurrentImages(currentPage));
        } catch (IOException e) {
            e.printStackTrace();
        }


        RecyclerView.LayoutManager layoutManager = new GridLayoutManager(getApplicationContext(), 7);
        //LinearLayoutManager layoutManager = new GridLayoutManager(getApplicationContext(), 7);

        image_recyclerView.setLayoutManager(layoutManager);
        image_recyclerView.setItemAnimator(new DefaultItemAnimator());
        image_recyclerView.addItemDecoration(new ItemOffsetDecoration(this, R.dimen.item_offset));
        image_recyclerView.setAdapter(imageAdapter);
    }

    private ArrayList<String> loadPhotosFromNativeGallery() {
        final String[] columns = {MediaStore.Images.Media.DATA, MediaStore.Images.Media._ID};
        final String orderBy = MediaStore.Images.Media.DATE_TAKEN;
        Cursor imagecursor = managedQuery(
                MediaStore.Images.Media.EXTERNAL_CONTENT_URI, columns, null,
                null, orderBy + " DESC");

        ArrayList<String> imageUrls = new ArrayList<String>();

        for (int i = 0; i < imagecursor.getCount(); i++) {
            imagecursor.moveToPosition(i);
            int dataColumnIndex = imagecursor.getColumnIndex(MediaStore.Images.Media.DATA);
            imageUrls.add(imagecursor.getString(dataColumnIndex));

            //System.out.println("=====> Array path => "+imageUrls.get(i));
        }

        return imageUrls;
    }


    private boolean mayRequestGalleryImages() {

        if (Build.VERSION.SDK_INT < Build.VERSION_CODES.M) {
            return true;
        }

        if (checkSelfPermission(READ_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED) {
            return true;
        }

        if (shouldShowRequestPermissionRationale(READ_EXTERNAL_STORAGE)) {
            //promptStoragePermission();
            showPermissionRationaleSnackBar();
        } else {
            requestPermissions(new String[]{READ_EXTERNAL_STORAGE}, REQUEST_FOR_STORAGE_PERMISSION);
        }

        return false;
    }

    private void showPermissionRationaleSnackBar() {
        Snackbar.make(findViewById(R.id.back), getString(R.string.permission_rationale),
                Snackbar.LENGTH_INDEFINITE).setAction(getResources().getString(R.string.OK), new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                // Request the permission
                ActivityCompat.requestPermissions(ImageListActicvity.this,
                        new String[]{READ_EXTERNAL_STORAGE},
                        REQUEST_FOR_STORAGE_PERMISSION);
            }
        }).show();

    }


    private Handler disconnectHandler = new Handler(new Handler.Callback() {
        @Override
        public boolean handleMessage(Message msg) {
            // todo
            return true;
        }
    });

    private Runnable disconnectCallback = new Runnable() {
        @Override
        public void run() {
            // Perform any required operation on disconnect
            Intent newIntent = new Intent(ImageListActicvity.this, AdsActivity.class);
            startActivity(newIntent);
        }
    };

    public void resetDisconnectTimer() {
        disconnectHandler.removeCallbacks(disconnectCallback);
        disconnectHandler.postDelayed(disconnectCallback, AppText.DISCONNECT_TIMEOUT);
    }

    public void stopDisconnectTimer() {
        disconnectHandler.removeCallbacks(disconnectCallback);
    }

    @Override
    public void onUserInteraction() {
        resetDisconnectTimer();
    }

    @Override
    public void onResume() {
        super.onResume();
        resetDisconnectTimer();
    }

    @Override
    public void onStop() {
        super.onStop();
        stopDisconnectTimer();
    }

    private void initView() {

        loadMore = (Button) findViewById(R.id.loadMore);

        intent = getIntent();
        subCategoryName = intent.getStringExtra("fileName");
        categoryName = intent.getStringExtra("categoryName");

        Log.d("Category ", categoryName);
        Log.d("SubCategory : ", subCategoryName);

        try {
            p = new Paginator();
            totalPages = p.getTotalPages();
        } catch (IOException e) {
            e.printStackTrace();
        }
        toggleButtons();


        back = (ImageView) findViewById(R.id.back);
        activity_name = (TextView) findViewById(R.id.activity_name);
        activity_name.setText(categoryName + " > " + subCategoryName);
        no_image_textView = (TextView) findViewById(R.id.no_image_textView);
        no_image_textView.setVisibility(View.GONE);
        image_recyclerView = (RecyclerView) findViewById(R.id.image_recyclerView);
        //image_recyclerView.setVisibility(View.GONE);
    }

    private void clickMethods() {

        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });


        loadMore.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                currentPage += 1;
                // enableDisableButtons();
                try {
                    imageAdapter.addImage(p.getCurrentImages(currentPage));
                    imageAdapter.notifyDataSetChanged();
                } catch (IOException e) {
                    e.printStackTrace();
                }
                toggleButtons();

            }
        });

    }

    private void toggleButtons() {
        //SINGLE PAGE DATA
        if (totalPages <= 0) {
            loadMore.setVisibility(View.GONE);
        }
        //LAST PAGE
        else if (currentPage == totalPages) {
            loadMore.setVisibility(View.GONE);
        }
        //FIRST PAGE
        else if (currentPage == 0) {
            loadMore.setVisibility(View.VISIBLE);
        }
        //SOMEWHERE IN BETWEEN
        else if (currentPage >= 1 && currentPage <= totalPages) {
            loadMore.setVisibility(View.VISIBLE);
        }

    }


    public class Paginator {
        ArrayList<String> allImages = DataHolder.fetchFiles(categoryName, subCategoryName, ImageListActicvity.this);
        public final int TOTAL_NUM_ITEMS = allImages.size();
        public static final int ITEMS_PER_PAGE = 20;

        public Paginator() throws IOException {
        }

        public int getTotalPages() {
            int remainingItems = TOTAL_NUM_ITEMS % ITEMS_PER_PAGE;
            if (remainingItems > 0) {
                System.out.println(TOTAL_NUM_ITEMS / ITEMS_PER_PAGE);
                return TOTAL_NUM_ITEMS / ITEMS_PER_PAGE;
            }
            System.out.println((TOTAL_NUM_ITEMS / ITEMS_PER_PAGE) - 1);
            return (TOTAL_NUM_ITEMS / ITEMS_PER_PAGE) - 1;

        }


        public ArrayList<String> getCurrentImages(int currentPage) throws IOException {
            int startItem = currentPage * ITEMS_PER_PAGE;
            int lastItem = startItem + ITEMS_PER_PAGE;

            ArrayList<String> currentImages = new ArrayList<>();


            //LOOP THRU LIST OF GALAXIES AND FILL CURRENTGALAXIES LIST
            try {
                for (int i = 0; i < allImages.size(); i++) {

                    //ADD CURRENT PAGE'S DATA
                    if (i >= startItem && i < lastItem) {
                        currentImages.add(allImages.get(i));
                    }
                }
                return currentImages;
            } catch (Exception e) {
                e.printStackTrace();
                return null;
            }
        }
    }
}


