package com.prizmadigital.shresthatailoring.model;

import java.util.ArrayList;

public class CategoryDTO {
    String categoryId;
    int categoryImage;
    String categoryName;
    ArrayList<SubCategoryDTO> subCategoryList = new ArrayList<>();

    public CategoryDTO(String categoryId, int categoryImage, String categoryName, ArrayList<SubCategoryDTO> subCategoryList) {
        this.categoryId = categoryId;
        this.categoryImage = categoryImage;
        this.categoryName = categoryName;
        this.subCategoryList = subCategoryList;
    }


    public int getCategoryImage() {
        return categoryImage;
    }

    public void setCategoryImage(int categoryImage) {
        this.categoryImage = categoryImage;
    }

    public String getCategoryName() {
        return categoryName;
    }

    public void setCategoryName(String categoryName) {
        this.categoryName = categoryName;
    }

    public String getCategoryId() {
        return categoryId;
    }

    public void setCategoryId(String categoryId) {
        this.categoryId = categoryId;
    }

    public ArrayList<SubCategoryDTO> getSubCategoryList() {
        return subCategoryList;
    }

    public void setSubCategoryList(ArrayList<SubCategoryDTO> subCategoryList) {
        this.subCategoryList = subCategoryList;
    }
}
