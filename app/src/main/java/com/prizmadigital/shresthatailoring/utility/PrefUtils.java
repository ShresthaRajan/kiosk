package com.prizmadigital.shresthatailoring.utility;

import android.content.Context;
import android.content.SharedPreferences;

public class PrefUtils {

    public static final String SHARED_PREF_SHRESTHA_TAILORING = "shrestha_tailoring";
    public static final String GRANTED_PERMISSION = "granted_permission";
    public static final String SELECTED_VIDEO_NAME = "selected_video_name";

    public static void saveEnabledPermission(Context context, boolean permission) {
        SharedPreferences prefs = context.getSharedPreferences(SHARED_PREF_SHRESTHA_TAILORING, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = prefs.edit();
        editor.putBoolean(GRANTED_PERMISSION, permission);
        editor.commit();
    }

    public static Boolean enabledPermission(Context context) {
        SharedPreferences pref = context.getSharedPreferences(SHARED_PREF_SHRESTHA_TAILORING, Context.MODE_PRIVATE);
        return pref.getBoolean(GRANTED_PERMISSION, false);
    }

    public static void saveVideoName(Context context, String video_name) {
        SharedPreferences prefs = context.getSharedPreferences(SHARED_PREF_SHRESTHA_TAILORING, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = prefs.edit();
        editor.putString(SELECTED_VIDEO_NAME, video_name);
        editor.commit();
    }

    public static String getVideoName(Context context) {
        SharedPreferences pref = context.getSharedPreferences(SHARED_PREF_SHRESTHA_TAILORING, Context.MODE_PRIVATE);
        return pref.getString(SELECTED_VIDEO_NAME, "shrestha_tailoring1.mp4");
    }
}
