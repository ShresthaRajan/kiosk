package com.prizmadigital.shresthatailoring.activity;

import android.media.MediaPlayer;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.widget.MediaController;
import android.widget.Toast;
import android.widget.VideoView;


import com.prizmadigital.shresthatailoring.R;
import com.prizmadigital.shresthatailoring.utility.AppText;
import com.prizmadigital.shresthatailoring.utility.PrefUtils;
import com.prizmadigital.shresthatailoring.utility.Utilities;

import java.io.File;
import java.io.IOException;

public class AdsActivity extends AppCompatActivity {


    private final Handler handler = new Handler();
    Runnable runnable;
    Boolean mBooleanIsPressed;


    private String VIDEO_SAMPLE = "shrestha_tailoring1.mp4";

    VideoView videoView;


    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_ads);

        videoView = (VideoView) findViewById(R.id.videoView);
        VIDEO_SAMPLE = PrefUtils.getVideoName(AdsActivity.this);

        MediaController controller = new MediaController(this);
        controller.setAnchorView(videoView);
        controller.setMediaPlayer(videoView);
        videoView.setMediaController(controller);
        videoView.start();

        videoView.setOnCompletionListener(new MediaPlayer.OnCompletionListener() {
            @Override
            public void onCompletion(MediaPlayer mediaPlayer) {
                videoView.start();
            }
        });

    }


    @Override
    public void onUserInteraction() {
        finish();
    }

    private Uri getMedia(String mediaName) throws IOException {
        /*return Uri.parse("android.resource://" + getPackageName() +
                "/raw/" + mediaName);*/

        Uri actualVideoPath;
        String path = AppText.AppPath + "/video";
        if (Utilities.checkPathIsAvailable(AdsActivity.this, new File(AppText.AppPath + "/video"))) {

            //Toast.makeText(this, "File path available", Toast.LENGTH_SHORT).show();
            File file = new File(path + "/" + mediaName);
            if (file.exists()) {
                //Toast.makeText(this, "File  available", Toast.LENGTH_SHORT).show();
                actualVideoPath = Uri.parse(path + "/" + mediaName);
            } else {
                //  Toast.makeText(this, "File not available", Toast.LENGTH_SHORT).show();
                actualVideoPath = Uri.parse("android.resource://" + getPackageName() + "/" + R.raw.shrestha_tailoring1);
            }

        } else {
            Utilities.makeDirectory(AdsActivity.this, new File(AppText.AppPath + "/video"));
            //Toast.makeText(this, "File path created", Toast.LENGTH_SHORT).show();
            File file = new File(path + "/" + mediaName);
            if (file.exists()) {
                //Toast.makeText(this, "File  available", Toast.LENGTH_SHORT).show();
                actualVideoPath = Uri.parse(path + "/" + mediaName);
            } else {
                //Toast.makeText(this, "File not available", Toast.LENGTH_SHORT).show();
                actualVideoPath = Uri.parse("android.resource://" + getPackageName() + "/" + R.raw.shrestha_tailoring1);
            }
        }

        //Toast.makeText(this, "File to play  " + actualVideoPath, Toast.LENGTH_SHORT).show();
        return actualVideoPath;
    }

    private void initializePlayer() throws IOException {
        Uri videoUri = getMedia(VIDEO_SAMPLE);
        videoView.setVideoURI(videoUri);
    }

    private void releasePlayer() {
        videoView.stopPlayback();
    }

    @Override
    protected void onStart() {
        super.onStart();

        try {
            initializePlayer();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @Override
    protected void onStop() {
        super.onStop();

        releasePlayer();
    }

    @Override
    protected void onPause() {
        super.onPause();

        if (Build.VERSION.SDK_INT < Build.VERSION_CODES.N) {
            videoView.pause();
        }
    }
}
