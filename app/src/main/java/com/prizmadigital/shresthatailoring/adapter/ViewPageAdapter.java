package com.prizmadigital.shresthatailoring.adapter;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.support.annotation.NonNull;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.ProgressBar;

import com.bumptech.glide.Glide;
//import com.bumptech.glide.load.resource.drawable.GlideDrawable;
import com.bumptech.glide.request.RequestListener;
import com.bumptech.glide.request.target.Target;
import com.prizmadigital.shresthatailoring.R;
import com.prizmadigital.shresthatailoring.utility.photoview.PhotoView;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.util.ArrayList;

import javax.sql.DataSource;

public class ViewPageAdapter extends PagerAdapter {

    //conatsnt tags
    public static String TAG = ViewPageAdapter.class.getSimpleName();

    //variables
    private ArrayList<String> list;

    private LayoutInflater inflater;
    private Context mContext;

    //views
    //View myImageLayout = null;

    public ViewPageAdapter(Context context, ArrayList<String> imgList) {
        this.mContext = context;
        this.list = imgList;
        inflater = LayoutInflater.from(context);
    }


    @Override
    public void destroyItem(ViewGroup container, int position, Object object) {
        container.removeView((View) object);
    }

    @Override
    public int getCount() {
        return list.size();
    }


    @Override
    public Object instantiateItem(ViewGroup view, int position) {

        inflater = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View v = inflater.inflate(R.layout.view_pager_list_item, null);

        //myImageLayout = inflater.inflate(R.layout.view_pager_list_item, view, false);


        final PhotoView myImage = v.findViewById(R.id.image);

        final ProgressBar progressBar = (ProgressBar) v.findViewById(R.id.activitiesProgressBar);
        // String url = singleGalleryList.get(position).getImage_location()+singleGalleryList.get(position).getTitle();
        //myImage.setScaleType(ImageView.ScaleType.FIT_XY);


       /* Glide.with(mContext)
                .load("file://" + list.get(position))
                //.placeholder(R.drawable.logo)
                //.error(R.drawable.logo)
                .into(myImage);*/

        /*Glide.with(mContext).load("file://" + list.get(position)).error(R.drawable.logo).listener(new RequestListener<String, GlideDrawable>() {
            @Override
            public boolean onException(Exception e, String model, Target<GlideDrawable> target, boolean isFirstResource) {
                progressBar.setVisibility(View.GONE);
                return false;
            }

            @Override
            public boolean onResourceReady(GlideDrawable resource, String model, Target<GlideDrawable> target, boolean isFromMemoryCache, boolean isFirstResource) {
//                   Log.d(TAG,"Visibility: "+progressBar.getVisibility());
                progressBar.setVisibility(View.GONE);
                return false;
            }
        }).into(myImage);*/


       /* Glide.with(mContext).asBitmap().load(list.get(position)).into(new CustomTarget<Bitmap>() {
            @Override
            public void onResourceReady(Bitmap bitmap, Transition<? super Bitmap> transition) {
                myImage.setImageBitmap(bitmap);
                progressBar.setVisibility(View.GONE);
            }
            @Override
            public void onLoadCleared(Drawable placeholder) {
            }
        });*/

        myImage.setImageBitmap(decodeFile(new File(list.get(position))));
        ViewPager vp = (ViewPager)view;
        vp.addView(v, 0);

/*

        Glide.with(mContext)
                .load(list.get(position))
                .override(200, 300)
                .listener(new RequestListener<String, GlideDrawable>() {
                    @Override
                    public boolean onException(Exception e, String model, Target<GlideDrawable> target, boolean isFirstResource) {
                        progressBar.setVisibility(View.GONE);
                        return false;
                    }

                    @Override
                    public boolean onResourceReady(GlideDrawable resource, String model, Target<GlideDrawable> target, boolean isFromMemoryCache, boolean isFirstResource) {
                        progressBar.setVisibility(View.GONE);
                        return false;
                    }
                })
                .into(myImage);
*/

        //view.addView(myImageLayout, 0);


        //return myImageLayout;
        return v;
    }

    @Override
    public void destroyItem(@NonNull View container, int position, @NonNull Object object) {
        //super.destroyItem(container, position, object);
       /* ViewPager viewPager  =(ViewPager) container;
        View v = (View) object;
        viewPager.removeView(v);*/

        View view = (View) object;
        ImageView imageView = (ImageView) view.findViewById(R.id.image);
        Drawable drawable = imageView.getDrawable();
        if(drawable instanceof BitmapDrawable) {
            BitmapDrawable bitmapDrawable = (BitmapDrawable) drawable;
            if(bitmapDrawable != null) {
                Bitmap bitmap = bitmapDrawable.getBitmap();
                if(bitmap != null && !bitmap.isRecycled()) bitmap.recycle();
            }
        }
        ((ViewPager) container).removeView(view);

    }

    @Override
    public boolean isViewFromObject(View view, Object object) {
        return view.equals(object);
    }


    //decodes image and scales it to reduce memory consumption
    private Bitmap decodeFile(File f) {
        try {
            //Decode image size
            BitmapFactory.Options o = new BitmapFactory.Options();
            o.inJustDecodeBounds = true;
            BitmapFactory.decodeStream(new FileInputStream(f), null, o);

            //The new size we want to scale to
            final int REQUIRED_SIZE = 550;

            //Find the correct scale value. It should be the power of 2.
            int scale = 1;
            while (o.outWidth / scale / 2 >= REQUIRED_SIZE && o.outHeight / scale / 2 >= REQUIRED_SIZE)
                scale *= 2;

            //Decode with inSampleSize
            //BitmapFactory.Options o2 = new BitmapFactory.Options();
            //o2.inSampleSize = scale;
            o.inSampleSize = scale;
            o.inJustDecodeBounds = false;
            o.inPurgeable = true;
            return BitmapFactory.decodeStream(new FileInputStream(f), null, o);
        } catch (FileNotFoundException e) {
        }
        return null;
    }
}

