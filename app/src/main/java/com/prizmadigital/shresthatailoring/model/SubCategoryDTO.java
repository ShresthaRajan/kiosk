package com.prizmadigital.shresthatailoring.model;

import android.os.Parcel;
import android.os.Parcelable;

public class SubCategoryDTO implements Parcelable {
    String subcategoryId;
    String subcategoryName;


    public SubCategoryDTO(String subcategoryId, String subcategoryName) {
        this.subcategoryId = subcategoryId;
        this.subcategoryName = subcategoryName;
    }


    protected SubCategoryDTO(Parcel in) {
        subcategoryId = in.readString();
        subcategoryName = in.readString();
    }

    public static final Creator<SubCategoryDTO> CREATOR = new Creator<SubCategoryDTO>() {
        @Override
        public SubCategoryDTO createFromParcel(Parcel in) {
            return new SubCategoryDTO(in);
        }

        @Override
        public SubCategoryDTO[] newArray(int size) {
            return new SubCategoryDTO[size];
        }
    };

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(subcategoryId);
        dest.writeString(subcategoryName);
    }


    public String getSubcategoryId() {
        return subcategoryId;
    }

    public void setSubcategoryId(String subcategoryId) {
        this.subcategoryId = subcategoryId;
    }

    public String getSubcategoryName() {
        return subcategoryName;
    }

    public void setSubcategoryName(String subcategoryName) {
        this.subcategoryName = subcategoryName;
    }

    public static Creator<SubCategoryDTO> getCREATOR() {
        return CREATOR;
    }
}
