package com.prizmadigital.shresthatailoring.activity;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.prizmadigital.shresthatailoring.R;
import com.prizmadigital.shresthatailoring.adapter.FileRecyclerView;
import com.prizmadigital.shresthatailoring.interfaces.ItemClickListener;
import com.prizmadigital.shresthatailoring.model.SubCategoryDTO;
import com.prizmadigital.shresthatailoring.utility.AppText;
import com.prizmadigital.shresthatailoring.utility.Utilities;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;

public class SubCategoyListActivity extends AppCompatActivity {


    //view
    /*toolbar section*/
    ImageView back;
    TextView activity_name;
    TextView no_directory_textView;
    RecyclerView directory_recyclerView;


    //instances
    Intent intent;
    FileRecyclerView folderAdapter;

    //variables
    String categoryName;
    ArrayList<SubCategoryDTO> subList = new ArrayList<>();


    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_subcategory_list);

        initView();

        try {
            createSubCategoryFolder();
        } catch (IOException e) {
            e.printStackTrace();
        }

        clickMethods();

        resetDisconnectTimer();
    }


    private Handler disconnectHandler = new Handler(new Handler.Callback() {
        @Override
        public boolean handleMessage(Message msg) {
            // todo
            return true;
        }
    });

    private Runnable disconnectCallback = new Runnable() {
        @Override
        public void run() {
            // Perform any required operation on disconnect
            Intent newIntent = new Intent(SubCategoyListActivity.this, AdsActivity.class);
            startActivity(newIntent);
        }
    };

    public void resetDisconnectTimer() {
        disconnectHandler.removeCallbacks(disconnectCallback);
        disconnectHandler.postDelayed(disconnectCallback, AppText.DISCONNECT_TIMEOUT);
    }

    public void stopDisconnectTimer() {
        disconnectHandler.removeCallbacks(disconnectCallback);
    }

    @Override
    public void onUserInteraction() {
        resetDisconnectTimer();
    }

    @Override
    public void onResume() {
        super.onResume();
        resetDisconnectTimer();
    }

    @Override
    public void onStop() {
        super.onStop();
        stopDisconnectTimer();
    }


    private void initView() {

        //get clicked file path
        intent = getIntent();
        categoryName = intent.getStringExtra("fileName");
        subList = intent.getParcelableArrayListExtra("subCategory");


        back = (ImageView) findViewById(R.id.back);
        activity_name = (TextView) findViewById(R.id.activity_name);
        activity_name.setText(categoryName);
        no_directory_textView = (TextView) findViewById(R.id.no_directory_textView);
        no_directory_textView.setVisibility(View.GONE);
        directory_recyclerView = (RecyclerView) findViewById(R.id.directory_recyclerView);
        directory_recyclerView.setVisibility(View.GONE);

    }


    private void openSubCategory(String folderName) {
        Intent newIntent = new Intent(this, ImageListActicvity.class);
        newIntent.putExtra("fileName", folderName);
        newIntent.putExtra("categoryName", categoryName);
        startActivity(newIntent);
    }


    private ArrayList<String> fetchFiles() throws IOException {

        ArrayList<String> filenames = new ArrayList<String>();
        String path = AppText.AppPath + "/" + categoryName;

        Log.d("PATH =======> ", path);

        File file = new File(path);
        Utilities.checkPathIsAvailable(SubCategoyListActivity.this, file);

        File directory = new File(path);
        File[] files = directory.listFiles();

        Log.d("files =======> ", "" + files.length);
        if (file.length() > 0) {
            no_directory_textView.setVisibility(View.GONE);
            directory_recyclerView.setVisibility(View.VISIBLE);
            for (int i = 0; i < files.length; i++) {

                String file_name = files[i].getName();
                Log.d("FileName ===> ", files[i].getName());
                // you can store name to arraylist and use it later
                filenames.add(file_name);
            }
        }

        return filenames;
    }

    private void showfolders(ArrayList<String> filenames) {

        folderAdapter = new FileRecyclerView(SubCategoyListActivity.this, filenames, new ItemClickListener() {

            @Override
            public void onCategoryClick(String fileName, ArrayList<SubCategoryDTO> subList) {
                /*try {
                    if (Utilities.checkPathIsAvailable(SubCategoyListActivity.this, path)) {
                        openCategory(fileName);
                    } else {
                        Utilities.makeDirectory(SubCategoyListActivity.this, path);
                        openCategory(fileName);
                    }
                } catch (IOException e) {
                    e.printStackTrace();
                }*/
            }

            @Override
            public void onSubCategoryClick(String fileName) {
                openSubCategory(fileName);
            }

            @Override
            public void longPressClick(String fileName) {
                /*File path = new File(AppText.AppPath + "/" + categoryName + "/" + fileName);

                try {
                    if (Utilities.checkPathIsAvailable(SubCategoyListActivity.this, path)) {
                        path.delete();
                        Toast.makeText(SubCategoyListActivity.this, "file to be deleted :" + fileName, Toast.LENGTH_SHORT).show();
                    }
                } catch (IOException e) {
                    e.printStackTrace();
                }*/
            }
        });

        directory_recyclerView.setLayoutManager(new GridLayoutManager(this, 4));
        directory_recyclerView.setAdapter(folderAdapter);
    }

    private void createSubCategoryFolder() throws IOException {

        if (subList.size() > 0) {
            no_directory_textView.setVisibility(View.GONE);
            directory_recyclerView.setVisibility(View.VISIBLE);
            for (int i = 0; i < subList.size(); i++) {
                File path = new File(AppText.AppPath + "/" + categoryName + "/" + subList.get(i).getSubcategoryName());

                if (Utilities.checkPathIsAvailable(SubCategoyListActivity.this, path)) {
                } else {
                    Utilities.makeDirectory(SubCategoyListActivity.this, path);
                }

            }

        } else {
            no_directory_textView.setVisibility(View.VISIBLE);
            directory_recyclerView.setVisibility(View.GONE);
        }

        //get subfolder list adn show ther list
        ArrayList<String> fileList = fetchFiles();
        showfolders(fileList);

    }


    private void clickMethods() {

        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
    }
}
