package com.prizmadigital.shresthatailoring.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.prizmadigital.shresthatailoring.R;
import com.prizmadigital.shresthatailoring.model.AboutDTO;

import java.util.ArrayList;

public class AboutAdapter extends RecyclerView.Adapter<AboutAdapter.MyViewHolder> {

    private ArrayList<AboutDTO> aboutList = new ArrayList<>();
    private Context mContext;

    public AboutAdapter(Context context, ArrayList<AboutDTO> list) {
        mContext = context;
        aboutList = new ArrayList<AboutDTO>();
        this.aboutList = list;
    }


    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.image_list_item, parent, false);

        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, final int position) {

        holder.imageView.setImageResource(aboutList.get(position).getAbout_image());


        /*holder.imageView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent newIntent = new Intent(mContext, ImageGalleyActivity.class);
                newIntent.putStringArrayListExtra("imageList", mImagesList);
                newIntent.putExtra("position", position);
                mContext.startActivity(newIntent);
            }
        });*/

    }

    @Override
    public int getItemCount() {
        return aboutList.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {

        public ImageView imageView;

        public MyViewHolder(View view) {
            super(view);
            imageView = (ImageView) view.findViewById(R.id.imageView1);
        }
    }
}
