package com.prizmadigital.shresthatailoring.activity;

import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.os.Message;
import android.support.annotation.Nullable;
import android.support.design.widget.Snackbar;
import android.support.v4.app.ActivityCompat;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.prizmadigital.shresthatailoring.R;
import com.prizmadigital.shresthatailoring.adapter.VideoAdapter;
import com.prizmadigital.shresthatailoring.utility.AppText;
import com.prizmadigital.shresthatailoring.utility.ImageFetch.ImageAdapter;
import com.prizmadigital.shresthatailoring.utility.ImageFetch.ItemOffsetDecoration;
import com.prizmadigital.shresthatailoring.utility.Utilities;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import static android.Manifest.permission.READ_EXTERNAL_STORAGE;

public class VideoListActivity extends AppCompatActivity {

    //views
    ImageView back;
    TextView activity_name;
    RecyclerView video_recyclerView;
    TextView no_video_textview;

    //variables

    private static final int REQUEST_FOR_STORAGE_PERMISSION = 123;
    private File file;
    private List<String> myList;

    //instance
    VideoAdapter videoAdapter;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_video_list);

        back = (ImageView) findViewById(R.id.back);
        activity_name = (TextView) findViewById(R.id.activity_name);
        activity_name.setVisibility(View.GONE);
        video_recyclerView = (RecyclerView) findViewById(R.id.video_recyclerView);
        no_video_textview = (TextView) findViewById(R.id.no_video_textview);
        no_video_textview.setVisibility(View.GONE);

        try {
            populateVideo();
        } catch (IOException e) {
            e.printStackTrace();
        }

        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });


        resetDisconnectTimer();

    }


    private Handler disconnectHandler = new Handler(new Handler.Callback() {
        @Override
        public boolean handleMessage(Message msg) {
            // todo
            return true;
        }
    });

    private Runnable disconnectCallback = new Runnable() {
        @Override
        public void run() {
            // Perform any required operation on disconnect
            Intent newIntent = new Intent(VideoListActivity.this, AdsActivity.class);
            startActivity(newIntent);
        }
    };

    public void resetDisconnectTimer() {
        disconnectHandler.removeCallbacks(disconnectCallback);
        disconnectHandler.postDelayed(disconnectCallback, AppText.DISCONNECT_TIMEOUT);
    }

    public void stopDisconnectTimer() {
        disconnectHandler.removeCallbacks(disconnectCallback);
    }

    @Override
    public void onUserInteraction() {
        resetDisconnectTimer();
    }

    @Override
    public void onResume() {
        super.onResume();
        resetDisconnectTimer();
    }

    @Override
    public void onStop() {
        super.onStop();
        stopDisconnectTimer();
    }

    //For Fetching Image into Grid Form
    private void populateVideo() throws IOException {
        if (!mayRequestGalleryImages()) {
            return;
        }

        //ArrayList<String> imageUrls = loadPhotosFromNativeGallery();
        ArrayList<String> imageUrls = fetchVideos();
        //Toast.makeText(this, "Length : " + imageUrls.size(), Toast.LENGTH_SHORT).show();
        if (imageUrls.size() > 0) {
            video_recyclerView.setVisibility(View.VISIBLE);
            no_video_textview.setVisibility(View.GONE);
            initializeRecyclerView(imageUrls);
        } else {
            video_recyclerView.setVisibility(View.GONE);
            no_video_textview.setVisibility(View.VISIBLE);
        }

    }

    private void initializeRecyclerView(ArrayList<String> imageUrls) {
        videoAdapter = new VideoAdapter(this, imageUrls);

        RecyclerView.LayoutManager layoutManager = new GridLayoutManager(getApplicationContext(), 4);

        video_recyclerView.setLayoutManager(layoutManager);
        video_recyclerView.setItemAnimator(new DefaultItemAnimator());
        video_recyclerView.addItemDecoration(new ItemOffsetDecoration(this, R.dimen.item_offset));
        video_recyclerView.setAdapter(videoAdapter);
    }


    private boolean mayRequestGalleryImages() {

        if (Build.VERSION.SDK_INT < Build.VERSION_CODES.M) {
            return true;
        }

        if (checkSelfPermission(READ_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED) {
            return true;
        }

        if (shouldShowRequestPermissionRationale(READ_EXTERNAL_STORAGE)) {
            //promptStoragePermission();
            showPermissionRationaleSnackBar();
        } else {
            requestPermissions(new String[]{READ_EXTERNAL_STORAGE}, REQUEST_FOR_STORAGE_PERMISSION);
        }

        return false;
    }

    private void showPermissionRationaleSnackBar() {
        Snackbar.make(findViewById(R.id.back), getString(R.string.permission_rationale),
                Snackbar.LENGTH_INDEFINITE).setAction(getResources().getString(R.string.OK), new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                // Request the permission
                ActivityCompat.requestPermissions(VideoListActivity.this,
                        new String[]{READ_EXTERNAL_STORAGE},
                        REQUEST_FOR_STORAGE_PERMISSION);
            }
        }).show();

    }


    private ArrayList<String> fetchVideos() throws IOException {

        ArrayList<String> filenames = new ArrayList<String>();
        String path = AppText.AppPath + "/" + "video" + "/";

        Log.d("PATH =======> ", path);

        File file = new File(path);
        Utilities.checkPathIsAvailable(VideoListActivity.this, file);

        File directory = new File(path);
        File[] files = directory.listFiles();

        Log.d("files =======> ", "" + files.length);
        if (file.length() > 0) {
            no_video_textview.setVisibility(View.GONE);
            video_recyclerView.setVisibility(View.VISIBLE);
            for (int i = 0; i < files.length; i++) {

                String file_name = path + files[i].getName();
                Log.d("FileName ===> ", files[i].getName());
                // you can store name to arraylist and use it later
                filenames.add(file_name);
            }
        }

        return filenames;
    }
}
