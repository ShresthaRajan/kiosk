package com.prizmadigital.shresthatailoring.activity;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.prizmadigital.shresthatailoring.R;
import com.prizmadigital.shresthatailoring.adapter.OutletAdapter;
import com.prizmadigital.shresthatailoring.model.OutletDTO;
import com.prizmadigital.shresthatailoring.utility.AppText;
import com.prizmadigital.shresthatailoring.utility.ImageFetch.ItemOffsetDecoration;

import java.util.ArrayList;

public class OutletsActivity extends AppCompatActivity {

    //views
    ImageView back;
    TextView activity_name;
    RecyclerView outlet_recyclerView;

    //instance
    OutletAdapter outletAdapter;

    //variable
    ArrayList<OutletDTO> outletList = new ArrayList<>();

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_outlets);

        back = (ImageView) findViewById(R.id.back);
        activity_name = (TextView) findViewById(R.id.activity_name);
        activity_name.setVisibility(View.GONE);
        outlet_recyclerView = (RecyclerView) findViewById(R.id.outlet_recyclerView);

        getOutletList();

        setOutletList();

        //click Method
        clickMethod();

        resetDisconnectTimer();
    }

    private Handler disconnectHandler = new Handler(new Handler.Callback() {
        @Override
        public boolean handleMessage(Message msg) {
            // todo
            return true;
        }
    });

    private Runnable disconnectCallback = new Runnable() {
        @Override
        public void run() {
            // Perform any required operation on disconnect
            Intent newIntent = new Intent(OutletsActivity.this, AdsActivity.class);
            startActivity(newIntent);
        }
    };

    public void resetDisconnectTimer() {
        disconnectHandler.removeCallbacks(disconnectCallback);
        disconnectHandler.postDelayed(disconnectCallback, AppText.DISCONNECT_TIMEOUT);
    }

    public void stopDisconnectTimer() {
        disconnectHandler.removeCallbacks(disconnectCallback);
    }

    @Override
    public void onUserInteraction() {
        resetDisconnectTimer();
    }

    @Override
    public void onResume() {
        super.onResume();
        resetDisconnectTimer();
    }

    @Override
    public void onStop() {
        super.onStop();
        stopDisconnectTimer();
    }


    private void getOutletList() {
        outletList.add(new OutletDTO("1", "Aradhana Complex, Bagbazar Sadak", "+977 1 4225421, +977 1 4229603", R.drawable.outlets1, "27.7055272", "85.3214553"));
        outletList.add(new OutletDTO("2", "Chabahil Chowk", "+977 4465205", R.drawable.outlets2, "27.7310336", "85.3434795"));
        outletList.add(new OutletDTO("3", "Ground Floor, KL Tower, Chucchepati", "+977 4486893", R.drawable.outlets3, "27.7184192", "85.3498049"));
        outletList.add(new OutletDTO("4", "United World Trade Center, Tripureshwor", "+977 4117136", R.drawable.outlets1, "27.694255", "85.3130222"));
        outletList.add(new OutletDTO("5", "B.K Shrestha Bhawan, Maiti Devi", "+977 4414288", R.drawable.outlets4, "27.7037431", "85.3335345"));
    }


    private void setOutletList() {
        outletAdapter = new OutletAdapter(this, outletList);

        RecyclerView.LayoutManager layoutManager = new GridLayoutManager(getApplicationContext(), 3);

        outlet_recyclerView.setLayoutManager(layoutManager);
        outlet_recyclerView.setItemAnimator(new DefaultItemAnimator());
        outlet_recyclerView.addItemDecoration(new ItemOffsetDecoration(this, R.dimen.item_offset));
        outlet_recyclerView.setAdapter(outletAdapter);

    }

    private void clickMethod() {

        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });
    }

}
