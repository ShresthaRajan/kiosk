package com.prizmadigital.shresthatailoring.model;

public class OfferSliderDTO {
    String offer_id;
    int offer_image;
    String bannerpath;

    public OfferSliderDTO(String offer_id, int offer_image) {
        this.offer_id = offer_id;
        this.offer_image = offer_image;
    }

    public OfferSliderDTO(String offer_id, String bannerpath) {
        this.offer_id = offer_id;
        this.bannerpath = bannerpath;
    }

    public String getOffer_id() {
        return offer_id;
    }

    public void setOffer_id(String offer_id) {
        this.offer_id = offer_id;
    }

    public int getOffer_image() {
        return offer_image;
    }

    public void setOffer_image(int offer_image) {
        this.offer_image = offer_image;
    }

    public String getBannerpath() {
        return bannerpath;
    }

    public void setBannerpath(String bannerpath) {
        this.bannerpath = bannerpath;
    }
}

