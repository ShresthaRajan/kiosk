package com.prizmadigital.shresthatailoring.utility;

import android.app.Activity;
import android.util.Log;
import android.view.View;

import com.prizmadigital.shresthatailoring.activity.ImageListActicvity;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;

public class DataHolder {

    public static ArrayList<String> fetchFiles(String categoryName, String subCategoryName, Activity activity) throws IOException {

        ArrayList<String> filenames = new ArrayList<String>();
        String path = AppText.AppPath + "/" + categoryName + "/" + subCategoryName+"/";

        //Log.d("PATH =======> ", path);

        File file = new File(path);
        Utilities.checkPathIsAvailable(activity, file);

        File directory = new File(path);
        File[] files = directory.listFiles();

        Log.d("files =======> ", "" + files.length);
        if (file.length() > 0) {
            //ImageListActicvity.no_image_textView.setVisibility(View.GONE);
            //ImageListActicvity.image_recyclerView.setVisibility(View.VISIBLE);
            for (int i = 0; i < files.length; i++) {

                String file_name = path + files[i].getName();
                //Log.d("FileName ===> ", files[i].getName());
                // you can store name to arraylist and use it later
                filenames.add(file_name);
            }
        }

        return filenames;
    }
}


