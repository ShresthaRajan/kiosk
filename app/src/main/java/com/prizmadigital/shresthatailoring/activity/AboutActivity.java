package com.prizmadigital.shresthatailoring.activity;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.prizmadigital.shresthatailoring.R;
import com.prizmadigital.shresthatailoring.adapter.AboutAdapter;
import com.prizmadigital.shresthatailoring.model.AboutDTO;
import com.prizmadigital.shresthatailoring.utility.AppText;
import com.prizmadigital.shresthatailoring.utility.ImageFetch.ItemOffsetDecoration;

import java.util.ArrayList;

public class AboutActivity extends AppCompatActivity {

    //view
    ImageView back;
    TextView activity_name;
    RecyclerView about_recyclerView;

    //instance
    AboutAdapter aboutAdapter;


    //variables
    ArrayList<AboutDTO> aboutList = new ArrayList<>();


    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_about);

        about_recyclerView = (RecyclerView) findViewById(R.id.about_recyclerView);
        activity_name = (TextView) findViewById(R.id.activity_name);
        activity_name.setVisibility(View.GONE);
        back = (ImageView) findViewById(R.id.back);
        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });

        getAboutList();
        setAbout();

        resetDisconnectTimer();

    }

    private Handler disconnectHandler = new Handler(new Handler.Callback() {
        @Override
        public boolean handleMessage(Message msg) {
            // todo
            return true;
        }
    });

    private Runnable disconnectCallback = new Runnable() {
        @Override
        public void run() {
            // Perform any required operation on disconnect
            Intent newIntent = new Intent(AboutActivity.this, AdsActivity.class);
            startActivity(newIntent);
        }
    };

    public void resetDisconnectTimer() {
        disconnectHandler.removeCallbacks(disconnectCallback);
        disconnectHandler.postDelayed(disconnectCallback, AppText.DISCONNECT_TIMEOUT);
    }

    public void stopDisconnectTimer() {
        disconnectHandler.removeCallbacks(disconnectCallback);
    }

    @Override
    public void onUserInteraction() {
        resetDisconnectTimer();
    }

    @Override
    public void onResume() {
        super.onResume();
        resetDisconnectTimer();
    }

    @Override
    public void onStop() {
        super.onStop();
        stopDisconnectTimer();
    }



    private void getAboutList() {
        aboutList.add(new AboutDTO("1",R.drawable.about1));
        aboutList.add(new AboutDTO("2",R.drawable.about2));
        aboutList.add(new AboutDTO("3",R.drawable.about3));
        aboutList.add(new AboutDTO("4",R.drawable.about4));
        aboutList.add(new AboutDTO("5",R.drawable.about5));
        aboutList.add(new AboutDTO("6",R.drawable.about6));
        aboutList.add(new AboutDTO("7",R.drawable.about7));

    }


    private void setAbout() {

        aboutAdapter = new AboutAdapter(this, aboutList);

        RecyclerView.LayoutManager layoutManager = new GridLayoutManager(getApplicationContext(), 4);

        about_recyclerView.setLayoutManager(layoutManager);
        about_recyclerView.setItemAnimator(new DefaultItemAnimator());
        about_recyclerView.addItemDecoration(new ItemOffsetDecoration(this, R.dimen.item_offset));
        about_recyclerView.setAdapter(aboutAdapter);
    }
}
