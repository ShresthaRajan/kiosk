package com.prizmadigital.shresthatailoring.activity;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.support.annotation.Nullable;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.prizmadigital.shresthatailoring.R;
import com.prizmadigital.shresthatailoring.adapter.ScrollListAdapter;
import com.prizmadigital.shresthatailoring.adapter.ViewPageAdapter;
import com.prizmadigital.shresthatailoring.interfaces.RecylerItemClickListner;
import com.prizmadigital.shresthatailoring.utility.AppText;
import com.prizmadigital.shresthatailoring.utility.DataHolder;
import com.prizmadigital.shresthatailoring.utility.HackyViewPager;

import java.io.IOException;
import java.util.ArrayList;

public class ImageGalleyActivity extends AppCompatActivity implements RecylerItemClickListner {

    //instance
    Intent intent;
    ViewPageAdapter viewPageAdapter;
    ScrollListAdapter scrollListAdapter;

    //Variables
    int position;
    ArrayList<String> imageList = new ArrayList<>();

    //views
    ImageView back;
    TextView activity_name;
    private HackyViewPager gallery_viewPager;
    private ImageView image_gallery_ImageView;
    private RecyclerView image_detail_gallery_recyclerView;


    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_image_gallery);

        intent = getIntent();

        position = intent.getIntExtra("position", 0);
        imageList = intent.getStringArrayListExtra("imageList");


        back = (ImageView) findViewById(R.id.back);
        activity_name = (TextView) findViewById(R.id.activity_name);
        activity_name.setVisibility(View.GONE);
        gallery_viewPager = (HackyViewPager) findViewById(R.id.gallery_viewPager);
        image_gallery_ImageView = (ImageView) findViewById(R.id.image_gallery_ImageView);
        image_detail_gallery_recyclerView = (RecyclerView) findViewById(R.id.image_detail_gallery_recyclerView);

        setGalleyList(imageList, position);

        resetDisconnectTimer();

        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });
    }

    @Override
    protected void onStart() {
        super.onStart();
        //position = intent.getIntExtra("position", 0);
        gallery_viewPager.setCurrentItem(position);
        gallery_viewPager.setOffscreenPageLimit(5);
    }

    private Handler disconnectHandler = new Handler(new Handler.Callback() {
        @Override
        public boolean handleMessage(Message msg) {
            // todo
            return true;
        }
    });

    private Runnable disconnectCallback = new Runnable() {
        @Override
        public void run() {
            // Perform any required operation on disconnect
            Intent newIntent = new Intent(ImageGalleyActivity.this, AdsActivity.class);
            startActivity(newIntent);
        }
    };

    public void resetDisconnectTimer() {
        disconnectHandler.removeCallbacks(disconnectCallback);
        disconnectHandler.postDelayed(disconnectCallback, AppText.DISCONNECT_TIMEOUT);
    }

    public void stopDisconnectTimer() {
        disconnectHandler.removeCallbacks(disconnectCallback);
    }

    @Override
    public void onUserInteraction() {
        resetDisconnectTimer();
    }

    @Override
    public void onResume() {
        super.onResume();
        resetDisconnectTimer();
    }

    @Override
    public void onStop() {
        super.onStop();
        stopDisconnectTimer();
    }


    private void setGalleyList(ArrayList<String> imageList, int position) {

        if (imageList != null) {

            gallery_viewPager.setAdapter(new ViewPageAdapter(ImageGalleyActivity.this, imageList));
            /*gallery_viewPager.setCurrentItem(position);
            gallery_viewPager.setOffscreenPageLimit(5);*/


            gallery_viewPager.setOnPageChangeListener(new ViewPager.OnPageChangeListener() {
                @Override
                public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {
                    image_detail_gallery_recyclerView.scrollToPosition(position);
                }

                @Override
                public void onPageSelected(int position) {

                }

                @Override
                public void onPageScrollStateChanged(int scrollState) {
                    if (scrollState != ViewPager.SCROLL_STATE_IDLE) {
                        final int childCount = gallery_viewPager.getChildCount();
                        for (int i = 0; i < childCount; i++)
                            gallery_viewPager.getChildAt(i).setLayerType(View.LAYER_TYPE_NONE, null);
                    }
                }
            });


            if (imageList.size() == 1) {

                image_detail_gallery_recyclerView.setVisibility(View.GONE);
            } else {
                try {

                    image_detail_gallery_recyclerView.setHasFixedSize(true);
                    image_detail_gallery_recyclerView.setLayoutManager(new LinearLayoutManager(ImageGalleyActivity.this, LinearLayoutManager.VERTICAL, false));
                    //RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(getApplicationContext(),LinearLayoutManager.HORIZONTAL,false);
                    scrollListAdapter = new ScrollListAdapter(getLayoutInflater(), ImageGalleyActivity.this, position);
                    scrollListAdapter.addImageGallery(imageList);
                    image_detail_gallery_recyclerView.setAdapter(scrollListAdapter);
                    scrollListAdapter.setClickListener(ImageGalleyActivity.this);


                } catch (NullPointerException e) {
                    e.printStackTrace();
                }
            }
        }
    }


    @Override
    public void onClick(View view, int position) {
        gallery_viewPager.setCurrentItem(position);
    }
}
