package com.prizmadigital.shresthatailoring.activity;

import android.content.Intent;
import android.media.MediaPlayer;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.MediaController;
import android.widget.TextView;
import android.widget.VideoView;

import com.prizmadigital.shresthatailoring.R;
import com.prizmadigital.shresthatailoring.utility.AppText;
import com.prizmadigital.shresthatailoring.utility.PrefUtils;
import com.prizmadigital.shresthatailoring.utility.Utilities;

import java.io.File;
import java.io.IOException;

public class VideoPlayActivity extends AppCompatActivity {


    private static String VIDEO_SAMPLE = "";
    VideoView videoView;
    TextView video_tap_text;

    Intent intent;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_ads);

        intent = getIntent();
        VIDEO_SAMPLE = intent.getStringExtra("VideoURL");

        videoView = (VideoView) findViewById(R.id.videoView);
        video_tap_text = (TextView) findViewById(R.id.video_tap_text);
        video_tap_text.setText("Tap Video to play as ads");

        MediaController controller = new MediaController(this);
        controller.setAnchorView(videoView);
        controller.setMediaPlayer(videoView);
        videoView.setMediaController(controller);
        videoView.start();

        videoView.setOnCompletionListener(new MediaPlayer.OnCompletionListener() {
            @Override
            public void onCompletion(MediaPlayer mediaPlayer) {
                videoView.start();
            }
        });

        video_tap_text.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                PrefUtils.saveVideoName(VideoPlayActivity.this,VIDEO_SAMPLE.substring(VIDEO_SAMPLE.lastIndexOf("/") + 1));
                finish();
            }
        });

    }

    private Uri getMedia(String mediaName) throws IOException {

        Uri actualVideoPath;

        File file = new File(VIDEO_SAMPLE);
        if (file.exists()) {
            //Toast.makeText(this, "File  available", Toast.LENGTH_SHORT).show();
            actualVideoPath = Uri.parse(VIDEO_SAMPLE);
        } else {
            //  Toast.makeText(this, "File not available", Toast.LENGTH_SHORT).show();
            actualVideoPath = Uri.parse("android.resource://" + getPackageName() + "/" + R.raw.shrestha_tailoring1);
        }
        return actualVideoPath;
    }

    private void initializePlayer() throws IOException {
        Uri videoUri = getMedia(VIDEO_SAMPLE);
        videoView.setVideoURI(videoUri);
    }

    private void releasePlayer() {
        videoView.stopPlayback();
    }

    @Override
    protected void onStart() {
        super.onStart();

        try {
            initializePlayer();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @Override
    protected void onStop() {
        super.onStop();

        releasePlayer();
    }

    @Override
    protected void onPause() {
        super.onPause();

        if (Build.VERSION.SDK_INT < Build.VERSION_CODES.N) {
            videoView.pause();
        }
    }
}
