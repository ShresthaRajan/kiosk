package com.prizmadigital.shresthatailoring.activity;

import android.Manifest;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.os.Parcelable;
import android.os.StrictMode;
import android.support.annotation.Nullable;
import android.support.annotation.RequiresApi;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Toast;

import com.prizmadigital.shresthatailoring.R;
import com.prizmadigital.shresthatailoring.adapter.CategoryRecyclerView;
import com.prizmadigital.shresthatailoring.adapter.DashboardRecyclerView;
import com.prizmadigital.shresthatailoring.adapter.OfferSliderViewPagerAdapter;
import com.prizmadigital.shresthatailoring.interfaces.ItemClickListener;
import com.prizmadigital.shresthatailoring.model.CategoryDTO;
import com.prizmadigital.shresthatailoring.model.DashboardDTO;
import com.prizmadigital.shresthatailoring.model.OfferSliderDTO;
import com.prizmadigital.shresthatailoring.model.SubCategoryDTO;
import com.prizmadigital.shresthatailoring.utility.AppText;
import com.prizmadigital.shresthatailoring.utility.PrefUtils;
import com.prizmadigital.shresthatailoring.utility.Utilities;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Timer;
import java.util.TimerTask;

public class DashboardActivity extends AppCompatActivity {

    //views
    RecyclerView category_recyclerView;
    //RecyclerView dashboard_recyclerView;
    ViewPager offer_sliderViewPager;
    LinearLayout offer_sliderDots;
    LinearLayout about, outlet, feature_product;
    FloatingActionButton show_video_list;


    //instance
    CategoryRecyclerView categoryAdapter;
    DashboardRecyclerView dashboardAdapter;
    OfferSliderViewPagerAdapter offerSliderAdapter;

    //variables
    ArrayList<CategoryDTO> categoryList = new ArrayList<>();
    ArrayList<DashboardDTO> dashboardList = new ArrayList<>();
    public static final int PERMISSIONS_MULTIPLE_REQUEST = 123;
    Boolean premissionGranted = false;

    private int dotscount;


    private ImageView[] dots;
    Timer timer;
    int currentPosition = 0;
    ArrayList<OfferSliderDTO> offerList = new ArrayList<>();

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_dashboard);

        StrictMode.VmPolicy.Builder builder = new StrictMode.VmPolicy.Builder();
        StrictMode.setVmPolicy(builder.build());

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            requestRuntimePermission();
        }

        initView();
        getCategories();

        initOfferSlider();

        createSlideShow();

        /*getDashboardList();*/
        setListInDashboard();

        //click methods
        clickMethod();
        Utilities.createPathForApp(DashboardActivity.this, categoryList);

       /* final Handler handler = new Handler();
        handler.postDelayed(new Runnable() {
            @Override
            public void run() {
                // Do something after 5s = 5000ms
                Utilities.createPathForApp(DashboardActivity.this, categoryList);
            }
        }, 2000);*/

        resetDisconnectTimer();
    }


    private Handler disconnectHandler = new Handler(new Handler.Callback() {
        @Override
        public boolean handleMessage(Message msg) {
            // todo
            return true;
        }
    });

    private Runnable disconnectCallback = new Runnable() {
        @Override
        public void run() {
            // Perform any required operation on disconnect
            Intent newIntent = new Intent(DashboardActivity.this, AdsActivity.class);
            startActivity(newIntent);
        }
    };

    public void resetDisconnectTimer() {
        disconnectHandler.removeCallbacks(disconnectCallback);
        disconnectHandler.postDelayed(disconnectCallback, AppText.DISCONNECT_TIMEOUT);
    }

    public void stopDisconnectTimer() {
        disconnectHandler.removeCallbacks(disconnectCallback);
    }

    @Override
    public void onUserInteraction() {
        resetDisconnectTimer();
    }

    @Override
    public void onResume() {
        super.onResume();
        resetDisconnectTimer();
    }

    @Override
    public void onStop() {
        super.onStop();
        stopDisconnectTimer();
    }


    private void initView() {
        category_recyclerView = (RecyclerView) findViewById(R.id.category_recyclerView);
        //dashboard_recyclerView = (RecyclerView) findViewById(R.id.dashboard_recyclerView);
        offer_sliderViewPager = (ViewPager) findViewById(R.id.offer_sliderViewPager);
        offer_sliderDots = (LinearLayout) findViewById(R.id.offer_sliderDots);

        about = (LinearLayout) findViewById(R.id.about);
        outlet = (LinearLayout) findViewById(R.id.outlet);
        feature_product = (LinearLayout) findViewById(R.id.feature_product);
        show_video_list = (FloatingActionButton) findViewById(R.id.show_video_list);

        // ViewCompat.setNestedScrollingEnabled(dashboard_recyclerView, false);
    }


    private void getCategories() {
        String cateResponse = AppText.categoryResponse;
        try {
            JSONArray mainArray = new JSONArray(cateResponse);
            for (int i = 0; i < mainArray.length(); i++) {
                JSONObject categoryObject = mainArray.getJSONObject(i);


                String catId = "", catName = "";
                int catImage = 0;
                ArrayList<SubCategoryDTO> subList = new ArrayList<>();

                if (categoryObject.has("categoryId"))
                    catId = categoryObject.getString("categoryId");
                if (categoryObject.has("categoryName"))
                    catName = categoryObject.getString("categoryName");
                if (categoryObject.has("categoryImage"))
                    //catImage = categoryObject.getString("categoryImage");
                    if (i == 0) catImage = R.drawable.suits;
                if (i == 1) catImage = R.drawable.coats_and_blazers;
                if (i == 2) catImage = R.drawable.dress_shirts;
                if (i == 3) catImage = R.drawable.pants_and_accesories;
                if (i == 4) catImage = R.drawable.nepali_daura_surwal;
                if (i == 5) catImage = R.drawable.kids_blazzer;
                if (categoryObject.has("subcategory")) {
                    JSONArray subArray = categoryObject.getJSONArray("subcategory");
                    String subId = "", subName = "";
                    for (int j = 0; j < subArray.length(); j++) {
                        JSONObject subObject = subArray.getJSONObject(j);
                        if (subObject.has("subcategoryId"))
                            subId = subObject.getString("subcategoryId");
                        if (subObject.has("subcategoryName"))
                            subName = subObject.getString("subcategoryName");

                        subList.add(new SubCategoryDTO(subId, subName));
                    }

                    Log.d("Sub List size : ", "" + subList.size());
                }

                categoryList.add(new CategoryDTO(catId, catImage, catName, subList));

            }

        } catch (JSONException e) {
            e.printStackTrace();
        }


    }

    private void getDashboardList() {
        dashboardList.add(new DashboardDTO("1", R.drawable.coats_and_blazers, "Coats and Blazzers"));
        dashboardList.add(new DashboardDTO("2", R.drawable.dress_shirts, "dress shirts"));
        dashboardList.add(new DashboardDTO("3", R.drawable.pants_and_accesories, "Pant and Accessories"));
        dashboardList.add(new DashboardDTO("4", R.drawable.suits, "Suits"));
        dashboardList.add(new DashboardDTO("4", R.drawable.suits, "Suits"));
        dashboardList.add(new DashboardDTO("4", R.drawable.suits, "Suits"));
    }


    private void setListInDashboard() {
//        categoryAdapter = new CategoryRecyclerView(this);
//        categoryAdapter.addCategoryList(categoryList);

        categoryAdapter = new CategoryRecyclerView(DashboardActivity.this, categoryList, new ItemClickListener() {

            @RequiresApi(api = Build.VERSION_CODES.M)
            @Override
            public void onCategoryClick(String fileName, ArrayList<SubCategoryDTO> subList) {
                File path = new File(AppText.AppPath + "/" + fileName);
                try {
                    if (PrefUtils.enabledPermission(DashboardActivity.this)) {
                        if (Utilities.checkPathIsAvailable(DashboardActivity.this, path)) {
                            openCategory(fileName, subList);
                        } else {
                            Utilities.makeDirectory(DashboardActivity.this, path);
                            openCategory(fileName, subList);
                        }
                    } else {
                        requestRuntimePermission();
                    }

                } catch (IOException e) {
                    e.printStackTrace();
                }
                //Intent newIntent = new Intent(mContext, SubCategoyListActivity.class);
                //mContext.startActivity(newIntent);
            }

            @Override
            public void onSubCategoryClick(String fileName) {

            }

            @Override
            public void longPressClick(String fileName) {

            }

        });

        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getApplicationContext());
        category_recyclerView.setLayoutManager(mLayoutManager);
        category_recyclerView.setAdapter(categoryAdapter);


        /*dashboardAdapter = new DashboardRecyclerView(this);
        dashboardAdapter.addCategoryList(dashboardList);
        dashboard_recyclerView.setLayoutManager(new GridLayoutManager(this, 3));
        dashboard_recyclerView.setAdapter(dashboardAdapter);*/
    }

    private void openCategory(String folderName, ArrayList<SubCategoryDTO> subList) {
        Intent newIntent = new Intent(this, SubCategoyListActivity.class);
        newIntent.putExtra("fileName", folderName);
        newIntent.putParcelableArrayListExtra("subCategory", (ArrayList<? extends Parcelable>) subList);
        startActivity(newIntent);
    }

    private void initOfferSlider() {

        getOfferSliderList();

        final Handler handler = new Handler();
        handler.postDelayed(new Runnable() {
            @Override
            public void run() {
                // Do something after 5s = 5000ms

                offerSliderAdapter = new OfferSliderViewPagerAdapter(offerList, DashboardActivity.this);
                offer_sliderViewPager.setAdapter(offerSliderAdapter);

                dotscount = offerSliderAdapter.getCount();
                Toast.makeText(DashboardActivity.this, "offerlist : "+ offerList.size(), Toast.LENGTH_SHORT).show();

                if(dotscount != 0 && dotscount > 0){
                    dots = new ImageView[dotscount];

                    for (int i = 0; i < dotscount; i++) {

                        dots[i] = new ImageView(DashboardActivity.this);
                        dots[i].setImageDrawable(ContextCompat.getDrawable(DashboardActivity.this, R.drawable.offer_non_active_dot));

                        LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.WRAP_CONTENT);

                        params.setMargins(8, 0, 8, 0);

                        offer_sliderDots.addView(dots[i], params);

                    }
                    dots[0].setImageDrawable(ContextCompat.getDrawable(DashboardActivity.this, R.drawable.offer_active_dot));


                    offer_sliderViewPager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
                        @Override
                        public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

                        }

                        @Override
                        public void onPageSelected(int position) {

                            for (int i = 0; i < dotscount; i++) {
                                dots[i].setImageDrawable(ContextCompat.getDrawable(DashboardActivity.this, R.drawable.offer_non_active_dot));
                            }

                            dots[position].setImageDrawable(ContextCompat.getDrawable(DashboardActivity.this, R.drawable.offer_active_dot));

                        }

                        @Override
                        public void onPageScrollStateChanged(int state) {

                        }
                    });
                }
            }
        }, 1000);
    }



    private void getOfferSliderList() {

        String path = AppText.AppPath + "/Banner";
        offerList = new ArrayList<>();
        try {
            if (Utilities.checkPathIsAvailable(DashboardActivity.this, new File(AppText.AppPath + "/Banner"))) {
                fetchFiles(path);
            } else {
                offerList.add(new OfferSliderDTO("1", R.drawable.offer1));
                offerList.add(new OfferSliderDTO("1", R.drawable.offer2));
                offerList.add(new OfferSliderDTO("1", R.drawable.offer3));
                offerList.add(new OfferSliderDTO("1", R.drawable.offer4));
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private void clickMethod() {
        about.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent newIntent = new Intent(DashboardActivity.this, AboutActivity.class);
                startActivity(newIntent);
            }
        });

        outlet.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent newIntent = new Intent(DashboardActivity.this, OutletsActivity.class);
                startActivity(newIntent);
            }
        });

        feature_product.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent newIntent = new Intent(DashboardActivity.this, FeatureProductActivity.class);
                startActivity(newIntent);
            }
        });

        show_video_list.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent newIntent = new Intent(DashboardActivity.this, VideoListActivity.class);
                startActivity(newIntent);
            }
        });
    }

    private void createSlideShow() {

        final Handler handler = new Handler();
        final Runnable runnable = new Runnable() {
            @Override
            public void run() {

                if (currentPosition == dotscount)
                    currentPosition = 0;
                offer_sliderViewPager.setCurrentItem(currentPosition++, true);
            }
        };

        timer = new Timer();
        timer.schedule(new TimerTask() {
            @Override
            public void run() {
                handler.post(runnable);
            }
        }, 250, 2500);
    }


    //For run time permission
    @RequiresApi(api = Build.VERSION_CODES.M)
    public void requestRuntimePermission() {
        if (ContextCompat.checkSelfPermission(DashboardActivity.this,
                Manifest.permission.READ_EXTERNAL_STORAGE) +
                ContextCompat.checkSelfPermission(this,
                        Manifest.permission.WRITE_EXTERNAL_STORAGE)
                != PackageManager.PERMISSION_GRANTED) {

            if (ActivityCompat.shouldShowRequestPermissionRationale
                    (this, Manifest.permission.READ_EXTERNAL_STORAGE) ||
                    ActivityCompat.shouldShowRequestPermissionRationale
                            (this, Manifest.permission.WRITE_EXTERNAL_STORAGE)) {

                Snackbar.make(this.findViewById(android.R.id.content),
                        "Please Grant Permissions to access image",
                        Snackbar.LENGTH_INDEFINITE).setAction("ENABLE",
                        new View.OnClickListener() {
                            @RequiresApi(api = Build.VERSION_CODES.M)
                            @Override
                            public void onClick(View v) {
                                requestPermissions(
                                        new String[]{Manifest.permission
                                                .READ_EXTERNAL_STORAGE, Manifest.permission.CAMERA, Manifest.permission.WRITE_EXTERNAL_STORAGE},
                                        PERMISSIONS_MULTIPLE_REQUEST);
                                Utilities.createPathForApp(DashboardActivity.this, categoryList);
                                PrefUtils.saveEnabledPermission(DashboardActivity.this, true);

                            }
                        }).show();
            } else {
                requestPermissions(
                        new String[]{Manifest.permission
                                .READ_EXTERNAL_STORAGE, Manifest.permission.CAMERA, Manifest.permission.WRITE_EXTERNAL_STORAGE},
                        PERMISSIONS_MULTIPLE_REQUEST);
            }
        } else {
            // write your logic code if permission already granted
            Utilities.createPathForApp(DashboardActivity.this, categoryList);
            PrefUtils.saveEnabledPermission(DashboardActivity.this, true);
        }

    }

    @RequiresApi(api = Build.VERSION_CODES.M)
    @Override
    public void onRequestPermissionsResult(int requestCode,
                                           String permissions[], int[] grantResults) {
        switch (requestCode) {
            case PERMISSIONS_MULTIPLE_REQUEST: {
                // If request is cancelled, the result arrays are empty.
                if (grantResults.length > 0
                        && grantResults[0] == PackageManager.PERMISSION_GRANTED) {

                    // permission was granted, yay! Do the
                    // contacts-related task you need to do.
                    Utilities.createPathForApp(DashboardActivity.this, categoryList);
                    PrefUtils.saveEnabledPermission(DashboardActivity.this, true);

                } else {

                    // permission denied, boo! Disable the
                    // functionality that depends on this permission.
                    Toast.makeText(getApplicationContext(), "Permission denied", Toast.LENGTH_SHORT).show();
                    requestRuntimePermission();
                }
                return;
            }

            // other 'case' lines to check for other
            // permissions this app might request
        }
    }


    private void fetchFiles(String path) throws IOException {

        ArrayList<String> filenames = new ArrayList<String>();
        offerList = new ArrayList<>();

        File file = new File(path);
        Utilities.checkPathIsAvailable(DashboardActivity.this, file);

        File directory = new File(path);
        File[] files = directory.listFiles();

        if (files.length > 0) {
            for (int i = 0; i < files.length; i++) {

                String file_name = files[i].getName();
                // you can store name to arraylist and use it later
                filenames.add(file_name);
                offerList.add(new OfferSliderDTO("2", path + "/" +file_name));
            }
        } else if(files.length == 0){
            offerList.add(new OfferSliderDTO("1", R.drawable.offer1));
            offerList.add(new OfferSliderDTO("1", R.drawable.offer2));
            offerList.add(new OfferSliderDTO("1", R.drawable.offer3));
            offerList.add(new OfferSliderDTO("1", R.drawable.offer4));
        }

    }


}

