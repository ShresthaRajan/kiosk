package com.prizmadigital.shresthatailoring.utility;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.pm.ApplicationInfo;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.os.Environment;
import android.provider.MediaStore;

import com.prizmadigital.shresthatailoring.activity.DashboardActivity;
import com.prizmadigital.shresthatailoring.model.CategoryDTO;
import com.prizmadigital.shresthatailoring.model.SubCategoryDTO;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;

public class Utilities {

    public static void createPathForApp(Activity activity, ArrayList<CategoryDTO> categoryList) {
        File path = new File(AppText.AppPath);
        try {
            if (checkPathIsAvailable(activity, path)) {

                for (int i = 0; i < categoryList.size(); i++) {
                    makeCategoryDirectory(activity, categoryList.get(i));
                }

            } else {
                makeDirectory(activity, path);
                for (int i = 0; i < categoryList.size(); i++) {
                    makeCategoryDirectory(activity, categoryList.get(i));
                }
            }

            makeFeatureProductDirectory(activity);
            makeBannerDirectory(activity);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private static void makeCategoryDirectory(Activity activity, CategoryDTO categoryDTO) {
        File path = new File(AppText.AppPath + "/" + categoryDTO.getCategoryName());

        try {
            if (checkPathIsAvailable(activity, path)) {
                for (int i = 0; i < categoryDTO.getSubCategoryList().size(); i++) {
                    makeSubCategoryDirectory(activity, categoryDTO.getCategoryName(), categoryDTO.getSubCategoryList().get(i));
                }
            } else {
                makeDirectory(activity, path);
                for (int i = 0; i < categoryDTO.getSubCategoryList().size(); i++) {
                    makeSubCategoryDirectory(activity, categoryDTO.getCategoryName(), categoryDTO.getSubCategoryList().get(i));
                }

            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private static void makeFeatureProductDirectory(Activity activity) {
        File path = new File(AppText.AppPath + "/" + "Feature Product");

        try {
            if (checkPathIsAvailable(activity, path)) {

            } else {
                makeDirectory(activity, path);
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private static void makeBannerDirectory(Activity activity) {
        File path = new File(AppText.AppPath + "/" + "Banner");

        try {
            if (checkPathIsAvailable(activity, path)) {

            } else {
                makeDirectory(activity, path);
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private static void makeSubCategoryDirectory(Activity activity, String categoryName, SubCategoryDTO subCategoryDTO) {

        File path = new File(AppText.AppPath + "/" + categoryName + "/" + subCategoryDTO.getSubcategoryName());

        try {
            if (checkPathIsAvailable(activity, path)) {
            } else {
                makeDirectory(activity, path);
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public static Boolean checkPathIsAvailable(Activity activity, File file) throws IOException {
        boolean result = false;
        if (!file.exists()) {

            result = false;
        } else {
            result = true;
        }

        return result;
    }


    public static void makeDirectory(Activity activity, File file) throws IOException {
        file.mkdir();
        if (!file.exists()) {
            file.getParentFile().mkdir();
            file.createNewFile();
        }
    }

    public static boolean isGoogleMapsInstalled(Context context) {
        try {
            ApplicationInfo info = context.getPackageManager().getApplicationInfo("com.google.android.apps.maps", 0);
            return true;
        } catch (PackageManager.NameNotFoundException e) {
            return false;
        }
    }

    /*
     * isOnline - Check if there is a NetworkConnection
     * @return boolean
     */
    public static boolean isNetworkAvailable(Context context) {
        ConnectivityManager cm = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo netInfo = cm.getActiveNetworkInfo();
        if (netInfo != null && netInfo.isConnected()) {
            return true;
        } else {
            return false;
        }
    }

    public static void shareSelectedImage(Context context, String imagePath) {
//        Intent shareIntent = new Intent(android.content.Intent.ACTION_SEND);
//        shareIntent.setType("image/*");
//        //shareIntent.putExtra(Intent.EXTRA_STREAM, Uri.parse("file://" + "/sdcard/folder name/abc.png"));
//        shareIntent.putExtra(Intent.EXTRA_STREAM, Uri.parse(imagePath));
//        shareIntent.setFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION);
//        shareIntent.setPackage("com.prizmadigital.shresthatailoring");
//        context.startActivity(shareIntent);


        File file = new File(imagePath);
        Uri uri = Uri.fromFile(file);
        Intent intent = new Intent(Intent.ACTION_SEND);
        intent.setType("image/*");
        intent.putExtra(Intent.EXTRA_STREAM, uri);
        intent.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION);
        context.startActivity(intent);
    }
}
