package com.prizmadigital.shresthatailoring.adapter;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.Drawable;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.bumptech.glide.Glide;
import com.prizmadigital.shresthatailoring.R;
import com.prizmadigital.shresthatailoring.interfaces.RecylerItemClickListner;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.util.ArrayList;

public class ScrollListAdapter extends RecyclerView.Adapter<ScrollListAdapter.Holder> {

    Context mContext;
    Integer pos;
    private LayoutInflater mLayoutInflater;
    private ArrayList<String> imgList = new ArrayList<>();


    private RecylerItemClickListner clickListener;


    public ScrollListAdapter(LayoutInflater inflater, Context mContext, Integer pos) {
        mLayoutInflater = inflater;
        this.mContext = mContext;
        this.pos = pos;
    }


    @Override
    public Holder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = null;

        view = mLayoutInflater.inflate(R.layout.scroll_list_item, parent, false);
        return new Holder(view);
    }

    @Override
    public void onBindViewHolder(Holder holder, int position) {
        holder.bindImageGallery(imgList.get(position));
    }

    @Override
    public int getItemCount() {
        return imgList.size();
    }

    public void setClickListener(RecylerItemClickListner itemClickListener) {
        this.clickListener = itemClickListener;
    }


    public void addImageGallery(ArrayList<String> singleGalleryList) {
        imgList.addAll(singleGalleryList);
        notifyDataSetChanged();
    }


    public class Holder extends RecyclerView.ViewHolder implements View.OnClickListener {

        //for SliderGallery
        ImageView singleimage;
        private RecylerItemClickListner itemClickListner;


        public Holder(View itemView) {
            super(itemView);
            mContext = itemView.getContext();
            itemView.setOnClickListener(this);

            singleimage = (ImageView) itemView.findViewById(R.id.slider_detail_imageView);
            singleimage.setScaleType(ImageView.ScaleType.CENTER_CROP);

        }


        public void bindImageGallery(String imagePath) {
            //Glide.with(mContext).load(imagePath).error(R.drawable.logo).into(singleimage);
        /*    Glide.with(mContext).asBitmap().load(imagePath).into(new CustomTarget<Bitmap>() {
                @Override
                public void onResourceReady(Bitmap bitmap, Transition<? super Bitmap> transition) {
                    singleimage.setImageBitmap(bitmap);
                }
                @Override
                public void onLoadCleared(Drawable placeholder) {
                }
            });*/

            singleimage.setImageBitmap(decodeFile(new File(imagePath)));
        }


        @Override
        public void onClick(View view) {
            if (clickListener != null) clickListener.onClick(view, getAdapterPosition());
        }
    }

    //decodes image and scales it to reduce memory consumption
    private Bitmap decodeFile(File f){
        try {
            //Decode image size
            BitmapFactory.Options o = new BitmapFactory.Options();
            o.inJustDecodeBounds = true;
            BitmapFactory.decodeStream(new FileInputStream(f),null,o);

            //The new size we want to scale to
            final int REQUIRED_SIZE=550;

            //Find the correct scale value. It should be the power of 2.
            int scale=1;
            while(o.outWidth/scale/2>=REQUIRED_SIZE && o.outHeight/scale/2>=REQUIRED_SIZE)
                scale*=2;

            //Decode with inSampleSize
            //BitmapFactory.Options o2 = new BitmapFactory.Options();
            //o2.inSampleSize=scale;
            o.inSampleSize=scale;
            o.inJustDecodeBounds = false;
            o.inPurgeable = true;
            return BitmapFactory.decodeStream(new FileInputStream(f), null, o);
        } catch (FileNotFoundException e) {}
        return null;
    }
}
