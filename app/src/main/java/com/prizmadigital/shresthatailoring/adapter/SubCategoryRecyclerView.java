package com.prizmadigital.shresthatailoring.adapter;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.prizmadigital.shresthatailoring.R;
import com.prizmadigital.shresthatailoring.interfaces.ItemClickListener;
import com.prizmadigital.shresthatailoring.model.SubCategoryDTO;

import java.util.ArrayList;

public class SubCategoryRecyclerView extends RecyclerView.Adapter<SubCategoryRecyclerView.ViewHolder> {

    //view

    //variable
    public static String TAG = "categoryRecyclerView";
    Context mContext;
    ArrayList<SubCategoryDTO> subcategoryList = new ArrayList<>();
    ItemClickListener clickListener;


    public SubCategoryRecyclerView(Context mContext) {
        this.mContext = mContext;
    }

    public SubCategoryRecyclerView(Context mContext, ArrayList<SubCategoryDTO> subcategoryList, ItemClickListener itemClickListener) {
        this.mContext = mContext;
        this.subcategoryList = subcategoryList;
        this.clickListener = itemClickListener;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int i) {
        LayoutInflater inflater = LayoutInflater.from(parent.getContext());
        View view = inflater.inflate(R.layout.subcategory_list_item, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder viewHolder, int position) {
        viewHolder.bindCategory(subcategoryList.get(position), position);
    }

    @Override
    public int getItemCount() {
        return subcategoryList.size();
    }

    public void addCategoryList(ArrayList<SubCategoryDTO> list) {
        subcategoryList = new ArrayList<>();
        subcategoryList.addAll(list);
        notifyDataSetChanged();
    }

    public void clearCategory() {
        int size = subcategoryList.size();
        subcategoryList.clear();
        //notifyItemRangeRemoved(0, size);
        notifyDataSetChanged();
    }

    public ArrayList<SubCategoryDTO> returnCategoryList() {
        return subcategoryList;
    }


    public class ViewHolder extends RecyclerView.ViewHolder {

        /*
        *
          views for the recycler item
        *
        */

        LinearLayout subcategory_item;
        TextView subcategory_name;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            subcategory_item = (LinearLayout) itemView.findViewById(R.id.subcategory_item);
            subcategory_name = (TextView) itemView.findViewById(R.id.subcategory_name);
        }

        public void bindCategory(final SubCategoryDTO subcategoryDTO, final int position) {


            subcategory_name.setText(subcategoryDTO.getSubcategoryName());

           /* category_item.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    File path = new File(AppText.AppPath);
                    //if(Utilities.checkPathIsAvailable(,path));
                    //Intent newIntent = new Intent(mContext, SubCategoyListActivity.class);
                    //mContext.startActivity(newIntent);
                }
            });*/

            subcategory_item.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    clickListener.onSubCategoryClick(subcategoryDTO.getSubcategoryName());
                }
            });
        }


    }
}
